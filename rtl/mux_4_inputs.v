
module mux_4_inputs #(
    parameter INPUT_WIDTH = 5
  ) (
    input [4*INPUT_WIDTH-1:0] data_i,
    input [1:0]               selection_i,
    output [INPUT_WIDTH-1:0]  data_o
  );
  
  reg [INPUT_WIDTH-1:0] data_selection;

  always @(data_i or selection_i) begin
    case (selection_i)
      2'b00: data_selection <= data_i[INPUT_WIDTH-1:0];
      2'b01: data_selection <= data_i[2*INPUT_WIDTH-1:INPUT_WIDTH];
      2'b10: data_selection <= data_i[3*INPUT_WIDTH-1:2*INPUT_WIDTH];
      2'b11: data_selection <= data_i[4*INPUT_WIDTH-1:3*INPUT_WIDTH];
      default: data_selection <= data_i[INPUT_WIDTH-1:0];
    endcase
  end

  // Output assignment
  assign data_o = data_selection;
endmodule

