
module multiplier_bin_weight #(
    parameter W = 5   // input data bit size
  ) (
    input  [W-1:0] data_i,
    input          weight_i,
    output [W:0]   data_o
  );

  assign data_o = weight_i ? {1'b0, data_i} : -{1'b0, data_i};
endmodule
