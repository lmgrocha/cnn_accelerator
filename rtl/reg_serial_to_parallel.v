
module reg_serial_to_parallel #(
    parameter WIDTH = 32
  ) (
    input               clk_i,
    input               rst_i,
    input               data_i,
    input               write_i,
    output [WIDTH-1:0]  data_o
    );

  // Register data
  reg [WIDTH-1:0] register_data;

  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i)
      register_data <= {(WIDTH){1'b0}};
    else begin
      if (write_i)
        register_data <= {data_i, register_data[WIDTH-1:1]};
    end
  end
  
  // Set the output
  assign data_o = register_data;
endmodule
