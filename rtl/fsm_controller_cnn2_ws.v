
`include "definitions.v"

module fsm_controller_cnn2 #(
    parameter NUMBER_INPUT_CHANNELS = 32,     // Input feature map channels
    parameter INPUT_PROCESSING_LENGTH = 1000, // Number of input samples
    parameter FILTER_SIZE = 40,               // Filter size
    parameter NUMBER_FILTERS = 32,
    parameter RFW_ADDR_SIZE = 5,              // Address for filter loading
    parameter RFW_SIZE = 32,                  // Weight register file
    parameter FILTER_PAR = 40,                // Filter computation parallelism
    parameter NUMBER_PE_PAR = 8,              // Number of instantiated filters
    parameter LOG2_NUMBER_PE_PAR = 3,
    parameter PAR_INPUT_CHANNELS = 2,         // Number of input channels computed in parallel
    parameter OUTPUT_PAR = 1,                 // Number of outputs computed in parallel
    parameter LOG2_OUTPUT_PAR = 1
  ) (
    input                        clk_i,
    input                        rst_i,
    input                        start_config_i,
    input                        data_ready_i,
    output                       rfw_write_enable_o,
    output [RFW_ADDR_SIZE-1:0]   rfw_addr_o,
    output [RFW_ADDR_SIZE-1:0]   rfw_addr_channel_o,
    output                       ready_o,
    output [LOG2_OUTPUT_PAR-1:0] input_buffer_switch_o,
    output [OUTPUT_PAR-1:0]      write_accumulator_o,
    output                       enable_max_pool_o,
    output                       load_max_pool_o,
    output                       fetch_weight_o
  );

  /*
  * Internal parameters
  */
  localparam INPUT_COUNTER_SIZE = 2;
  localparam LOAD_CYCLE_COUNTER_SIZE = 1;

  // FSM parameters
  localparam NUMBER_STATES = 7;
  localparam SIZE_STAGE_REG = 3;
  localparam RESET = 0, LOAD_WEIGHTS = 1, IDLE = 2, PROCESS_FILTER = 3, 
             NEXT_INPUT_SEQ = 4, PROCESS_FILTER_MAX_POOL = 5, LOAD_NEXT_FILTER = 6;

  /*
  *	Start internal signals declaration
  */
  // Register file control
  reg                      rfw_write_enable;
  reg  [RFW_ADDR_SIZE-1:0] rfw_addr, rfw_addr_channel;
  reg                      rst_rfw_addr;
  wire                     enable_rfw_addr;
  reg                      enable_rfw_addr_channel;
  reg                      fetch_weight;
  
  // Input processing control
  reg  [INPUT_COUNTER_SIZE-1:0] input_seq_counter;
  reg                           rst_input_seq_counter, fetch_input_sequence;

  // When computing multiple outputs parallely, we need a counter
  `ifdef L2_USE_OUTPUT_PARALLEL
  reg  [LOG2_OUTPUT_PAR-1:0] output_process_counter;
  reg                        enable_output_process_counter, rst_output_process_counter;
  `endif

  // CNN Filter control
  reg  [RFW_ADDR_SIZE-1:0] filter_counter;
  reg                      rst_filter_counter, enable_filter_counter;

  // Accumulators control
  reg  [OUTPUT_PAR:0] write_accumulator;
  reg                 shift_write_accumulator;

  // Max pooling control
  reg enable_max_pool, load_max_pool;
  
  // FSM registers
  reg [SIZE_STAGE_REG-1:0] current_state, next_state;
  reg config_ready;

  /*
   *  FSM State change
   */
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i == 1'b1)
      current_state <= RESET;
    else
      current_state <= next_state;
  end

  /*
   *  FSM State Definition
   */
  always @(*) begin
    case (current_state)
      RESET: begin
        if (start_config_i)
          next_state <= LOAD_WEIGHTS;
        else
          next_state <= RESET;
      end
      LOAD_WEIGHTS: begin
        if ((rfw_addr == RFW_SIZE-1) && (rfw_addr_channel == RFW_SIZE-1))
          next_state <= IDLE;
        else
          next_state <= LOAD_WEIGHTS;
      end
      IDLE: begin 
        if (data_ready_i)
          next_state <= PROCESS_FILTER;
        else 
          next_state <= IDLE;
      end
      PROCESS_FILTER: begin
        `ifdef L2_USE_OUTPUT_PARALLEL
        if (output_process_counter == OUTPUT_PAR-1)
          next_state <= NEXT_INPUT_SEQ;
        else
          next_state <= PROCESS_FILTER;
        `else
          next_state <= NEXT_INPUT_SEQ;
        `endif
      end
      NEXT_INPUT_SEQ: begin
        if (input_seq_counter == (NUMBER_INPUT_CHANNELS/PAR_INPUT_CHANNELS-1))
          next_state <= PROCESS_FILTER_MAX_POOL;
        else
          next_state <= PROCESS_FILTER;
      end
      PROCESS_FILTER_MAX_POOL: begin
        `ifdef L2_USE_OUTPUT_PARALLEL
        if (output_process_counter == OUTPUT_PAR-1)
          next_state <= LOAD_NEXT_FILTER;
        else
          next_state <= PROCESS_FILTER_MAX_POOL;
        `else
          next_state <= LOAD_NEXT_FILTER;
        `endif
      end
      LOAD_NEXT_FILTER: begin
        if (filter_counter == RFW_SIZE-1)
          next_state <= IDLE;
        else
          next_state <= PROCESS_FILTER;
      end
      default: begin
        next_state <= RESET;
      end
    endcase
  end

  /*
   *  FSM Output Definition
   */
  always @(*) begin
    case (current_state)
      RESET: begin
        // Input processing control
        rst_input_seq_counter         <= 1'b1;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b1;
        enable_rfw_addr_channel       <= 1'b0;
        config_ready                  <= 1'b0;

        `ifdef L2_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b1;
        enable_output_process_counter <= 1'b0;
        `endif

        // Filter control
        rst_filter_counter            <= 1'b1;
        enable_filter_counter         <= 1'b0;

        // Accumulator control
        shift_write_accumulator       <= 1'b0;

        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      LOAD_WEIGHTS: begin
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b1;
        rfw_write_enable              <= 1'b1;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr_channel       <= 1'b1;
        config_ready                  <= 1'b0;
        
        `ifdef L2_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Filter control
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b0;

        // Accumulator control
        shift_write_accumulator       <= 1'b0;
        
        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      IDLE: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr_channel       <= 1'b0;
        config_ready                  <= 1'b1;
        
        `ifdef L2_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Filter control
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b0;

        // Accumulator control
        shift_write_accumulator       <= 1'b0;
        
        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      PROCESS_FILTER: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        config_ready                  <= 1'b1;

        // Control the operation to compute multiple outputs
        `ifdef L2_USE_OUTPUT_PARALLEL
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b1;
        `endif
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b0;
        enable_rfw_addr_channel       <= 1'b1;

        // Accumulator control
        shift_write_accumulator       <= 1'b1;
        
        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      NEXT_INPUT_SEQ: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b1;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr_channel       <= 1'b0;
        config_ready                  <= 1'b1;
        
        // Input channels sequence fetch control
        fetch_input_sequence          <= 1'b1;

        `ifdef L2_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Filter control
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b0;

        // Accumulator control
        shift_write_accumulator       <= 1'b0;
        
        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      PROCESS_FILTER_MAX_POOL: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        config_ready                  <= 1'b1;

        // Control the operation to compute multiple outputs
        `ifdef L2_USE_OUTPUT_PARALLEL
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b1;
        `endif
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b0;
        enable_rfw_addr_channel       <= 1'b0;

        // Accumulator control
        shift_write_accumulator       <= 1'b1;
        
        // Max pool control
        // TODO: check the timing as there is the accumulator delay
        enable_max_pool               <= 1'b1;
        `ifdef L2_USE_OUTPUT_PARALLEL
        if (output_process_counter == 0)
          load_max_pool               <= 1'b1;
        else
          load_max_pool               <= 1'b0;
        `endif
      end
      LOAD_NEXT_FILTER: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr_channel       <= 1'b0;
        config_ready                  <= 1'b1;

        // Parallel Output control
        `ifdef L2_USE_OUTPUT_PARALLEL
        rst_output_process_counter    <= 1'b1;
        enable_output_process_counter <= 1'b0;
        `endif

        // Filter control
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b1;

        // Accumulator control
        shift_write_accumulator       <= 1'b0;
        
        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      default: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr_channel       <= 1'b0;
        config_ready                  <= 1'b0;

        // Parallel Output control
        `ifdef L2_USE_OUTPUT_PARALLEL
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Filter control
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b0;

        // Accumulator control
        shift_write_accumulator       <= 1'b0;
        
        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
    endcase
  end

  /*
   * Registers 
   */
  // Register file control
  assign enable_rfw_addr = (rfw_addr_channel == RFW_SIZE-1) ? 1'b1 : 1'b0;
  always @(posedge clk_i or posedge rst_rfw_addr) begin
    if (rst_rfw_addr == 1'b1)
      rfw_addr <= {(RFW_ADDR_SIZE){1'b0}};
    else if (enable_rfw_addr) begin
      if (rfw_addr == RFW_SIZE-1)
        rfw_addr <= {(RFW_ADDR_SIZE){1'b0}};
      else
        rfw_addr <= rfw_addr + 1'b1;
    end
  end

  always @(posedge clk_i or posedge rst_rfw_addr) begin
    if (rst_rfw_addr == 1'b1)
      rfw_addr_channel <= {(RFW_ADDR_SIZE){1'b0}};
    else if (enable_rfw_addr_channel) begin
      if (rfw_addr_channel == RFW_SIZE-1)
        rfw_addr_channel <= {(RFW_ADDR_SIZE){1'b0}};
      else
        if (rfw_write_enable)
          rfw_addr_channel <= rfw_addr_channel + 1'b1;
        else
          rfw_addr_channel <= rfw_addr_channel + PAR_INPUT_CHANNELS;
    end
  end

  // Input counter control
  always @(posedge clk_i or posedge rst_input_seq_counter) begin
    if (rst_input_seq_counter == 1'b1)
      input_seq_counter <= {(INPUT_COUNTER_SIZE){1'b0}};
    else if (fetch_input_sequence)
      input_seq_counter <= input_seq_counter + 1;
  end

  // Filter control
  always @(posedge clk_i or posedge rst_filter_counter) begin
    if (rst_filter_counter == 1'b1)
      filter_counter <= {(RFW_ADDR_SIZE){1'b0}};
    else if (enable_filter_counter)
      filter_counter <= filter_counter + 1'b1;
  end
 
  `ifdef L2_USE_OUTPUT_PARALLEL
  // Output parallel control
  always @(posedge clk_i) begin
    if (rst_output_process_counter == 1'b1)
      output_process_counter <= {(LOG2_OUTPUT_PAR){1'b0}};
    else if (enable_output_process_counter)
      output_process_counter <= output_process_counter + 1'b1;
  end
  `endif

  // Since we have to accumulate the results, we need to select when to write
  // on the accumulation registers
  // TODO: check how to make this completely generic
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i == 1'b1)
      write_accumulator <= {{(OUTPUT_PAR){1'b0}}, 1'b1};
    else if (shift_write_accumulator)
      write_accumulator <= {write_accumulator[OUTPUT_PAR-1:0], write_accumulator[OUTPUT_PAR]};
  end

  /*
   * Output assignment
   */
  // TODO: check this
  assign fetch_weight_o       = fetch_weight;
  assign rfw_write_enable_o   = rfw_write_enable;
  assign rfw_addr_o           = rfw_addr;
  assign rfw_addr_channel_o   = rfw_addr_channel;
  assign ready_o              = config_ready;
  assign write_accumulator_o  = write_accumulator[OUTPUT_PAR:1];
  assign load_max_pool_o      = load_max_pool;
  assign enable_max_pool_o    = enable_max_pool;

  `ifdef L2_USE_OUTPUT_PARALLEL
  assign input_buffer_switch_o = output_process_counter;
  `else
  assign input_buffer_switch_o = 0;
  `endif
endmodule
