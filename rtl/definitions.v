
// Check if input data should be loaded in a multi-cycle approach
`define INPUT_LOAD_CYCLES 1

// Network parameters
`define PPG_WORD_WIDTH           5
`define INPUT_PROCESSING_LENGTH  60 // 1000
`define L1_FILTER_SIZE           5  // 40
`define L1_NUMBER_FILTERS        32
`define L2_FILTER_SIZE           5  // 40
`define L2_NUMBER_FILTERS        32

// CNN Layer 1 structure
`define L1_RFW_SIZE         `L1_NUMBER_FILTERS
`define L1_RFW_ADDR_SIZE    5
`define L1_FILTER_PAR       5 // `L1_FILTER_SIZE
`define L1_LOG2_FILTER_PAR  3 // 6
`define L1_OUTPUT_PAR       4 // 4
`define L1_LOG2_OUTPUT_PAR  2 // 2
`define PPG_INTERFACE_WIDTH `PPG_WORD_WIDTH*(`L1_FILTER_SIZE+`L1_OUTPUT_PAR-1)
// TODO: OPTIMIZE THIS
`define L1_USE_OUTPUT_PARALLEL 1

// BN + Binarizer threshold definitions
`define BN_RFW_SIZE         `L1_NUMBER_FILTERS
`define BN_RFW_ADDR_SIZE    5
`define BN_THRESHOLD_WIDTH  8

// CNN Layer 2 structure
`define L2_INPUT_CHANNELS        `L1_NUMBER_FILTERS
`define L2_LOG2_INPUT_CHANNELS   5
`define L2_RFW_SIZE              `L2_NUMBER_FILTERS
`define L2_RFW_ADDR_SIZE         5
`define L2_FILTER_PAR            5 // `L2_FILTER_SIZE
`define L2_LOG2_FILTER_PAR       3 // 6
`define L2_NUMBER_PE_PAR         8
`define L2_LOG2_NUMBER_PE_PAR    2 // 3
`define L2_PAR_INPUT_CHANNELS    2
`define L2_OUTPUT_PAR            1 
`define L2_LOG2_OUTPUT_PAR       1
`define L2_PAR_ACC_PROC          `L2_NUMBER_PE_PAR/`L2_PAR_INPUT_CHANNELS

// TODO: OPTIMIZE THIS
//`define L2_USE_OUTPUT_PARALLEL 1
