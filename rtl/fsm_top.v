
`include "definitions.v"

module fsm_top_controller 
  (
    input                           clk_i,
    input                           rst_i,
    input                           start_config_i,
    input                           ready_cnn1_i,
    input                           ready_cnn2_i,
    output                          weight_source_o,
    output                          config_cnn1_o,
    output                          config_cnn2_o,
    output                          load_bn1_threshold_o,
    output                          load_bn2_threshold_o,
    output                          bn_addr_source_o,
    output [`BN_RFW_ADDR_SIZE-1:0]  bn_addr_o,
    output                          done_config_o
  );

  // FSM parameters
  localparam NUMBER_STATES = 6;
  localparam SIZE_STAGE_REG = 3;
  localparam RESET = 0, CONFIG_CNN1 = 1, CONFIG_BN1_THRESHOLD = 2, CONFIG_CNN2 = 3, 
             CONFIG_BN2_THRESHOLD = 4, CONFIG_DONE = 5;
  
  /*
  *	Start internal signals declaration
  */
  reg weight_source;

  // CNN1 controls
  reg config_cnn1;

  // CNN2 controls
  reg config_cnn2;

  // BN register bank signals
  reg  [`BN_RFW_ADDR_SIZE-1:0] bn_addr;
  reg                          rst_bn_addr, enable_bn_addr;
  reg                          bn_addr_source, load_bn1_threshold, load_bn2_threshold;

  // FSM registers
  reg [SIZE_STAGE_REG-1:0] current_state, next_state;
  reg finish_all_configs;

  /*
   *  FSM State change
   */
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i == 1'b1)
      current_state <= RESET;
    else
      current_state <= next_state;
  end

  /*
   *  FSM State Definition
   */
  always @(*) begin
    case (current_state)
      RESET: begin
        if (start_config_i)
          next_state <= CONFIG_CNN1;
        else
          next_state <= RESET;
      end
      CONFIG_CNN1: begin
        if (ready_cnn1_i)
          next_state <= CONFIG_BN1_THRESHOLD;
        else
          next_state <= CONFIG_CNN1;
      end
      CONFIG_BN1_THRESHOLD: begin
        if (bn_addr == `BN_RFW_SIZE-1)
          next_state <= CONFIG_CNN2;
        else
          next_state <= CONFIG_BN1_THRESHOLD;
      end
      CONFIG_CNN2: begin
        if (ready_cnn2_i)
          next_state <= CONFIG_BN2_THRESHOLD;
        else
          next_state <= CONFIG_CNN2;
      end
      CONFIG_BN2_THRESHOLD: begin
        if (bn_addr == `BN_RFW_SIZE-1)
          next_state <= CONFIG_DONE;
        else
          next_state <= CONFIG_BN2_THRESHOLD;
      end
      CONFIG_DONE: begin
        next_state <= CONFIG_DONE;
      end
      default: begin
        next_state <= RESET;
      end
    endcase
  end

  /*
   *  FSM Output Definition
   */
  always @(*) begin
    case (current_state)
      RESET: begin
        weight_source       <= 1'b0;

        // CNN1 control
        config_cnn1         <= 1'b0;

        // BN register control
        rst_bn_addr         <= 1'b1;
        enable_bn_addr      <= 1'b0;
        bn_addr_source      <= 1'b0;
        load_bn1_threshold  <= 1'b0;
        load_bn2_threshold  <= 1'b0;
        
        // CNN2 control
        config_cnn2         <= 1'b0;

        // global config flag
        finish_all_configs  <= 1'b0;
      end
      CONFIG_CNN1: begin
        weight_source       <= 1'b0;

        // CNN1 control
        config_cnn1         <= 1'b1;

        // BN register control
        rst_bn_addr         <= 1'b0;
        enable_bn_addr      <= 1'b0;
        bn_addr_source      <= 1'b0;
        load_bn1_threshold  <= 1'b0;
        load_bn2_threshold  <= 1'b0;
        
        // CNN2 control
        config_cnn2         <= 1'b0;

        // global config flag
        finish_all_configs  <= 1'b0;
      end
      CONFIG_BN1_THRESHOLD: begin
        weight_source       <= 1'b0;
        
        // CNN1 control
        config_cnn1         <= 1'b0;

        // BN register control
        rst_bn_addr         <= 1'b0;
        enable_bn_addr      <= 1'b1;
        bn_addr_source      <= 1'b0;
        load_bn1_threshold  <= 1'b1;
        load_bn2_threshold  <= 1'b0;
        
        // CNN2 control
        config_cnn2         <= 1'b0;

        // global config flag
        finish_all_configs  <= 1'b0;
      end
      CONFIG_CNN2: begin
        weight_source       <= 1'b1;
 
        // CNN1 control
        config_cnn1         <= 1'b0;

        // BN register control
        rst_bn_addr         <= 1'b0;
        enable_bn_addr      <= 1'b0;
        bn_addr_source      <= 1'b0;
        load_bn1_threshold  <= 1'b0;
        load_bn2_threshold  <= 1'b0;
        
        // CNN2 control
        config_cnn2         <= 1'b1;

        // global config flag
        finish_all_configs  <= 1'b0;
      end
      CONFIG_BN2_THRESHOLD: begin
        weight_source       <= 1'b1;
        
        // CNN1 control
        config_cnn1         <= 1'b0;

        // BN register control
        rst_bn_addr         <= 1'b0;
        enable_bn_addr      <= 1'b1;
        bn_addr_source      <= 1'b0;
        load_bn1_threshold  <= 1'b0;
        load_bn2_threshold  <= 1'b1;
        
        // CNN2 control
        config_cnn2         <= 1'b0;

        // global config flag
        finish_all_configs  <= 1'b0;
      end
      CONFIG_DONE: begin
        weight_source       <= 1'b0;
 
        // CNN1 control
        config_cnn1         <= 1'b0;

        // BN register control
        rst_bn_addr         <= 1'b1;
        enable_bn_addr      <= 1'b0;
        bn_addr_source      <= 1'b1;
        load_bn1_threshold  <= 1'b0;
        load_bn2_threshold  <= 1'b0;
        
        // CNN2 control
        config_cnn2         <= 1'b0;

        // global config flag
        finish_all_configs  <= 1'b1;
      end
      default: begin
        weight_source       <= 1'b0;
 
        // CNN1 control
        config_cnn1         <= 1'b0;

        // BN1 register control
        rst_bn_addr         <= 1'b1;
        enable_bn_addr      <= 1'b0;
        bn_addr_source      <= 1'b0;
        load_bn1_threshold  <= 1'b0;
        load_bn2_threshold  <= 1'b0;
        
        // CNN2 control
        config_cnn2         <= 1'b0;

        // global config flag
        finish_all_configs  <= 1'b0;
      end
    endcase
  end

  /*
   * Registers 
   */
  // BN1 register bank control
  always @(posedge clk_i or posedge rst_bn_addr) begin
    if (rst_bn_addr == 1'b1)
      bn_addr <= {(`BN_RFW_ADDR_SIZE){1'b0}};
    else if (enable_bn_addr) begin
      if (bn_addr == `BN_RFW_SIZE-1)
        bn_addr <= {(`BN_RFW_ADDR_SIZE){1'b0}};
      else
        bn_addr <= bn_addr + 1'b1;
    end
  end

  /*
   *  Output assignment
   */
  assign weight_source_o      = weight_source;
  assign config_cnn1_o        = config_cnn1;
  assign config_cnn2_o        = config_cnn2;
  assign load_bn1_threshold_o = load_bn1_threshold;
  assign load_bn2_threshold_o = load_bn2_threshold;
  assign bn_addr_source_o     = bn_addr_source;
  assign bn_addr_o            = bn_addr;
  assign done_config_o        = finish_all_configs;
endmodule
