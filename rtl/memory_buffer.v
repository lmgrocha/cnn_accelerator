
module memory_buffer #(
    parameter WIDTH = 32,           // Number of filters
    parameter DEPTH = 40,           // Filter size
    parameter PARALLEL_EXT = 0,     // Number of extra values to store for each filter to consider parallel output on CNN2
    parameter PARALLEL_CHANNELS = 2 // Number of channels read in parallel
  ) (
    input                                               clk_i,
    input                                               rst_i,
    input                                               write_buffer_i,
    input                                               data_i,
    input                                               fetch_memory_data_i,
    output                                              data_ready_o,
    output reg [(DEPTH+PARALLEL_EXT)*PARALLEL_CHANNELS-1:0] data_o
  );
  
  // Read control
  localparam LOG2_WIDTH = 5;
  localparam LOG2_DEPTH = 6;
  localparam OUTPUT_SIZE = (DEPTH+PARALLEL_EXT)*PARALLEL_CHANNELS;
  reg  [LOG2_WIDTH-1:0] read_address_counter;
  reg  [LOG2_DEPTH-1:0] loaded_lines_counter;

  // Input buffer
  reg  [WIDTH-1:0] input_buffer;

  // Memory write counter
  reg  [LOG2_WIDTH-1:0] memory_write_counter;
  reg                   enable_memory_write;

  // Memory signals
  reg                             memory_array_in [0:WIDTH-1];
  wire [(DEPTH+PARALLEL_EXT)-1:0] memory_array_out [0:WIDTH-1];

  // Memory write control
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i) begin
      memory_write_counter  <= {(WIDTH){1'b0}};
      enable_memory_write   <= 1'b0;
    end else if (write_buffer_i) begin
      if (memory_write_counter == WIDTH-1) begin
        memory_write_counter  <= {(WIDTH){1'b0}};
        enable_memory_write   <= 1'b1;
      end else begin
        memory_write_counter  <= memory_write_counter + 1'b1;
        enable_memory_write   <= 1'b0;
      end
    end else begin
      memory_write_counter  <= memory_write_counter;
      enable_memory_write   <= 1'b0;
    end
  end

  // Input buffer writing
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i) begin
      input_buffer <= {WIDTH{1'b0}};
    end else begin
      if (write_buffer_i) 
        input_buffer <= {data_i, input_buffer[WIDTH-1:1]};
    end
  end
  
  // Read address control
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i)
      read_address_counter <= {(LOG2_WIDTH){1'b0}};
    else begin
      if ((loaded_lines_counter == (DEPTH+PARALLEL_EXT)) && fetch_memory_data_i)
        read_address_counter <= read_address_counter + PARALLEL_CHANNELS;
    end
  end
  
  // Loaded lines counter
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i)
      loaded_lines_counter <= {(LOG2_DEPTH){1'b0}};
    else begin
      if ((enable_memory_write) && (loaded_lines_counter < (DEPTH+PARALLEL_EXT)))
        loaded_lines_counter <= loaded_lines_counter + 1'b1;
    end
  end

  // Memory input routing signals
  integer indexColLC;
  always @(*) begin
    for (indexColLC = WIDTH-1; indexColLC >= 0; indexColLC = indexColLC-1) begin
      //Put the new data on the top
      memory_array_in[indexColLC] <= input_buffer[indexColLC];
    end
  end

  // Memory array instantiation
  genvar indexRowMemory, indexColMemory;
  generate
    for (indexColMemory = 0; indexColMemory < WIDTH; indexColMemory = indexColMemory+1) begin : gen_col
      reg_serial_to_parallel #(DEPTH+PARALLEL_EXT) reg_instance (
        .clk_i(clk_i),
        .rst_i(rst_i),
        .write_i(enable_memory_write),
        .data_i(memory_array_in[indexColMemory]),
        .data_o(memory_array_out[indexColMemory]));
    end
  endgenerate

  // Set the output
  genvar index_output;
  generate
    if (OUTPUT_SIZE >= 2) begin
      for (index_output = 0; index_output < PARALLEL_CHANNELS; index_output = index_output + 1) begin : gen_parallel_channels
        always @ (read_address_counter or data_ready_o)
          data_o[(index_output+1)*(DEPTH+PARALLEL_EXT)-1:index_output*(DEPTH+PARALLEL_EXT)] = {(DEPTH+PARALLEL_EXT){data_ready_o}} & memory_array_out[read_address_counter+index_output];
      end
    end else
      always @ (read_address_counter or data_ready_o)
        data_o = {(DEPTH+PARALLEL_EXT){data_ready_o}} & memory_array_out[read_address_counter];
  endgenerate

  assign data_ready_o = (loaded_lines_counter == (DEPTH+PARALLEL_EXT)) ? 1'b1 : 1'b0;
endmodule
