
module mux_2_inputs #(
    parameter INPUT_WIDTH = 5
  ) (
    input [2*INPUT_WIDTH-1:0] data_i,
    input                     selection_i,
    output [INPUT_WIDTH-1:0]  data_o
  );
  
  reg [INPUT_WIDTH-1:0] data_selection;

  always @(data_i or selection_i) begin
    case (selection_i)
      1'b0: data_selection <= data_i[INPUT_WIDTH-1:0];
      1'b1: data_selection <= data_i[2*INPUT_WIDTH-1:INPUT_WIDTH];
      default: data_selection <= data_i[INPUT_WIDTH-1:0];
    endcase
  end

  // Output assignment
  assign data_o = data_selection;
endmodule

