
module max_pool4 #(
    parameter W = 5		// register size
  ) (
    input                 clk_i,
    input                 rst_i,
    input                 load_i,
    input  signed [W-1:0] data_i,
    output signed [W-1:0] data_o
  );

  reg signed [W-1:0] reg_data [0:3];
  
  // Shift register to control the enable signals
  reg [3:0] enable_register;

  // Holders for comparison result
  wire signed [W-1:0] comparison_AB;
  wire signed [W-1:0] comparison_CD;

  // Declare input registers
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i) begin
      reg_data[0] <= {(W){1'b0}};
    end else begin
      if (load_i && enable_register[0])
        reg_data[0] <= data_i;
    end
  end

  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i) begin
      reg_data[1] <= {(W){1'b0}};
    end else begin
      if (load_i && enable_register[1])
        reg_data[1] <= data_i;
    end
  end

  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i) begin
      reg_data[2] <= {(W){1'b0}};
    end else begin
      if (load_i && enable_register[2])
        reg_data[2] <= data_i;
    end
  end

  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i) begin
      reg_data[3] <= {(W){1'b0}};
    end else begin
      if (load_i && enable_register[3])
        reg_data[3] <= data_i;
    end
  end

  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i) begin
      enable_register <= 4'b0001;
    end else begin
      if (load_i)
        enable_register <= {enable_register[2:0], enable_register[3]};
    end
  end

  // Perform the intermediary comparison
  assign comparison_AB = (reg_data[0] >= reg_data[1]) ? reg_data[0] : reg_data[1];
  assign comparison_CD = (reg_data[2] >= reg_data[3]) ? reg_data[2] : reg_data[3];
  
  // Perform the final comparison
  assign data_o = (comparison_AB >= comparison_CD) ? comparison_AB : comparison_CD;
endmodule
