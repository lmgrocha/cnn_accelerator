
`include "definitions.v"

module cnn_layer_1 #(
    parameter INPUT_WIDTH = `PPG_WORD_WIDTH,        // PPG input width
    parameter FILTER_SIZE = `L1_FILTER_SIZE,        // Filter size
    parameter NUMBER_FILTERS = `L1_NUMBER_FILTERS,
    parameter RFW_SIZE = `L1_RFW_SIZE,              // Weight register file
    parameter RFW_ADDR_SIZE = `L1_RFW_ADDR_SIZE,    // Address for filter loading
    parameter FILTER_PAR = `L1_FILTER_PAR,          // Filter computation parallelism
    parameter LOG2_FILTER_PAR = `L1_LOG2_FILTER_PAR,
    parameter OUTPUT_PAR = `L1_OUTPUT_PAR,          // Number of outputs computed in paralleli
    parameter LOG2_OUTPUT_PAR = `L1_LOG2_OUTPUT_PAR
  ) (
    input                               clk_i,
    input                               rst_i,
    input                               start_config_i,
    input                               new_window_i,
    input  [INPUT_WIDTH*(FILTER_SIZE+OUTPUT_PAR-1)-1:0] ppg_data_i,
    input  [FILTER_SIZE-1:0]            weight_i,
    output                              config_ready_o,
    output                              fetch_ppg_data_o,
    output [1:0]                        ppg_read_mode_o,
    output                              fetch_weight_o,
    output [RFW_ADDR_SIZE-1:0]          addr_filter_o,
    output [(INPUT_WIDTH+1)+LOG2_FILTER_PAR-1:0] accumulator_o,
    output                              enable_max_pool_o,
    output                              load_max_pool_o
  );

  // Create the define to determine the parallel output behaviour
  generate
    if (OUTPUT_PAR > 1) begin : declare_output_parallel
      `define L1_USE_OUTPUT_PARALLEL 1
    end
  endgenerate

  /*
   *  SIGNALS DECLARATION
   */
  // Register file signals
  wire [FILTER_SIZE-1:0]   rfw_data;
  wire [RFW_ADDR_SIZE-1:0] rfw_addr;
  wire                     rfw_write_enable;

  // Input buffer signals
  wire                                              parallel_load_buffer, shift_load_buffer;
  wire [INPUT_WIDTH*(FILTER_SIZE+OUTPUT_PAR-1)-1:0] buffer_data;
  wire [INPUT_WIDTH*(OUTPUT_PAR)-1:0]               serial_ppg_data;
  
  // Input mux shifters
  wire [INPUT_WIDTH-1:0]      shift_data [0:FILTER_PAR-1];
  wire [LOG2_OUTPUT_PAR-1:0]  shift_control;

  // Multiplier Signals
  wire [FILTER_PAR-1:0]  multiplier_weight_i;
  wire [INPUT_WIDTH:0]   multiplier_data_o [0:FILTER_PAR-1];
  wire [(INPUT_WIDTH+1)*FILTER_PAR-1:0] unpacked_output;
  
  // Adder tree
  wire [(INPUT_WIDTH+1)+LOG2_FILTER_PAR-1:0] adder_tree_o;

  // Output register signals
  wire [RFW_SIZE-1:0] write_output_register;
  
  // Max pool control
  wire  enable_max_pool, load_max_pool;
  
  /*
   *  STATE MACHINE DECLARATION
   */
  fsm_controller_cnn1 #(
    .INPUT_PROCESSING_LENGTH(`INPUT_PROCESSING_LENGTH),
    .FILTER_SIZE(FILTER_SIZE),
    .NUMBER_FILTERS(NUMBER_FILTERS),
    .RFW_ADDR_SIZE(RFW_ADDR_SIZE),
    .RFW_SIZE(RFW_SIZE),
    .FILTER_PAR(FILTER_PAR),
    .OUTPUT_PAR(OUTPUT_PAR),
    .LOG2_OUTPUT_PAR(LOG2_OUTPUT_PAR)
  ) i_fsm_controller (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .start_config_i(start_config_i),
    .new_window_i(new_window_i),
    .rfw_write_enable_o(rfw_write_enable),
    .rfw_addr_o(rfw_addr),
    .ready_o(config_ready_o),
    .parallel_load_buffer_o(parallel_load_buffer),
    .shift_load_buffer_o(shift_load_buffer),
    .input_buffer_switch_o(shift_control),
    .enable_max_pool_o(enable_max_pool),
    .load_max_pool_o(load_max_pool),
    .fetch_ppg_data_o(fetch_ppg_data_o),
    .fetch_ppg_mode_o(ppg_read_mode_o),
    .fetch_weight_o(fetch_weight_o)
  );

  /*
   * WEIGHT REGISTER FILE
   */
  register_file #(
    .DEPTH(RFW_SIZE),
    .ADDR_SIZE(RFW_ADDR_SIZE),
    .W(FILTER_SIZE)
  ) i_register_file_weights (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .write_i(rfw_write_enable),
    .addr_i(rfw_addr),
    .data_i(weight_i),
    .data_o(rfw_data)
  );

  /*
   * INPUT DATA BUFFER
   */
  // Share the same interface for parallel and serial PPG load
  assign serial_ppg_data = ppg_data_i[INPUT_WIDTH*(OUTPUT_PAR)-1:0];

  input_buffer #(
    .W(INPUT_WIDTH),
    .DEPTH(FILTER_SIZE),
    .SHIFT_SIZE(OUTPUT_PAR)
  ) i_input_buffer (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .parallel_load_i(parallel_load_buffer),
    .shift_load_i(shift_load_buffer),
    .parallel_data_i(ppg_data_i),
    .serial_data_i(serial_ppg_data),
    .data_o(buffer_data)
  );

  // Use input muxes to handle parallel output computation 
  genvar index_shift;
  generate
    if (OUTPUT_PAR == 2) begin
      for (index_shift = 0; index_shift < FILTER_PAR; index_shift = index_shift+1) begin : gen_shifter
        mux_2_inputs #(
          .INPUT_WIDTH(INPUT_WIDTH)
        ) mux_input_shift (
          .data_i(buffer_data[(index_shift+2)*INPUT_WIDTH-1:(index_shift)*INPUT_WIDTH]),
          .selection_i(shift_control),
          .data_o(shift_data[index_shift])
        );  
      end
    end else if (OUTPUT_PAR == 3) begin
      for (index_shift = 0; index_shift < FILTER_PAR; index_shift = index_shift+1) begin : gen_shifter
        mux_3_inputs #(
          .INPUT_WIDTH(INPUT_WIDTH)
        ) mux_input_shift (
          .data_i(buffer_data[(index_shift+3)*INPUT_WIDTH-1:(index_shift)*INPUT_WIDTH]),
          .selection_i(shift_control),
          .data_o(shift_data[index_shift])
        );  
      end
    end else if (OUTPUT_PAR == 4) begin
      for (index_shift = 0; index_shift < FILTER_PAR; index_shift = index_shift+1) begin : gen_shifter
        mux_4_inputs #(
          .INPUT_WIDTH(INPUT_WIDTH)
        ) mux_input_shift (
          .data_i(buffer_data[(index_shift+4)*INPUT_WIDTH-1:(index_shift)*INPUT_WIDTH]),
          .selection_i(shift_control),
          .data_o(shift_data[index_shift])
        );  
      end
    end else begin
      for (index_shift = 0; index_shift < FILTER_PAR; index_shift = index_shift+1) begin : gen_no_shifter
        assign shift_data[index_shift] = buffer_data[(index_shift+1)*INPUT_WIDTH-1:(index_shift)*INPUT_WIDTH];
      end
    end
  endgenerate

  /*
   * MULTIPLIER DECLARATION
   */
  genvar index_input;
  generate
    for (index_input = 0; index_input < FILTER_PAR; index_input = index_input+1) begin : gen_assign_mult_input
      assign unpacked_output[(index_input+1)*(INPUT_WIDTH+1)-1:index_input*(INPUT_WIDTH+1)] = multiplier_data_o[index_input];
    end
  endgenerate

  assign multiplier_weight_i = rfw_data;

  genvar index_mult;
  generate
    for (index_mult = 0; index_mult < FILTER_PAR; index_mult = index_mult + 1) begin : gen_mult
      multiplier_bin_weight #(
        .W(INPUT_WIDTH)
      ) i_multiplier_bin_weight (
        .data_i(shift_data[index_mult]),
        .weight_i(multiplier_weight_i[index_mult]),
        .data_o(multiplier_data_o[index_mult])
      );
    end
  endgenerate

  /*
   * ADDER TREE
   */
  adder_tree #(
    .W(INPUT_WIDTH+1),
    .NUMBER_INPUTS(FILTER_PAR),
    .LOG2_NUMBER_INPUTS(LOG2_FILTER_PAR)
  ) i_adder_tree (
    .data_i(unpacked_output),
    .data_o(adder_tree_o)
  );

  /*
   * OUTPUT REGISTERS
   */
  // XXX: This is not used if each filter kernel is computed entirely in parallel, so no accumulation is required
  //assign accumulation = register_selection + {adder_tree_o[INPUT_WIDTH+FILTER_PAR-1], adder_tree_o};

  /*
  genvar index_register;
  generate
    for (index_register = 0; index_register < RFW_SIZE; index_register = index_register + 1) begin : gen_register
      reg_n_bits #(
        .W(INPUT_WIDTH+FILTER_PAR)
      ) i_output_register (
        .clk_i(clk_i),
        .rst_i(rst_i),
        .enable_i(write_output_register[index_register]),
        .data_i(adder_tree_o),
        .data_o(output_registers[index_register])
      );
    end
  endgenerate
  */
  /*
   *  OUTPUT ASSIGNMENT
   */
  assign addr_filter_o      = rfw_addr;
  assign accumulator_o      = adder_tree_o;
  assign enable_max_pool_o  = enable_max_pool;
  assign load_max_pool_o    = load_max_pool;
endmodule
