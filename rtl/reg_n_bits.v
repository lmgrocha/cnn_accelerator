
module reg_n_bits #(
    parameter W = 5		// register size
  ) (
    input                 clk_i,
    input                 rst_i,
    input                 enable_i,
    input  signed [W-1:0] data_i,
    output signed [W-1:0] data_o
  );

  reg signed [W-1:0] reg_data;

  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i) begin
      reg_data <= {(W){1'b0}};
    end else begin
      if (enable_i)
        reg_data <= data_i;
    end
  end

  assign data_o = reg_data;
endmodule
