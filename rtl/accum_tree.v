
module accum_tree #(
    parameter W = 5,                  // Size inputs
    parameter NUMBER_INPUTS = 5,      // Number of inputs
    parameter OUTPUT_SIZE = 5
  ) (
    input  [W*NUMBER_INPUTS-1:0] data_i,
    output [OUTPUT_SIZE-1:0] data_o
  );

  // Sum tree
  reg  [OUTPUT_SIZE-1:0] sum;

  // Remap inputs
  integer index_input;

  always @* begin
    sum = 0;
    for (index_input = 0; index_input < NUMBER_INPUTS; index_input = index_input + 1) begin
      sum = $signed(sum) + $signed(data_i[index_input*W+:W]);
    end
  end
  
  assign data_o = sum;
endmodule

