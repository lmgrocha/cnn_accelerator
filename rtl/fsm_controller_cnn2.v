
`include "definitions.v"

module fsm_controller_cnn2 #(
    parameter NUMBER_INPUT_CHANNELS = 32,     // Input feature map channels
    parameter INPUT_PROCESSING_LENGTH = 1000, // Number of input samples
    parameter FILTER_SIZE = 40,               // Filter size
    parameter NUMBER_FILTERS = 32,
    parameter RFW_ADDR_SIZE = 5,              // Address for filter loading
    parameter RFW_SIZE = 32,                  // Weight register file
    parameter FILTER_PAR = 40,                // Filter computation parallelism
    parameter NUMBER_PE_PAR = 8,              // Number of instantiated filters
    parameter LOG2_NUMBER_PE_PAR = 3,
    parameter PAR_INPUT_CHANNELS = 2,         // Number of input channels computed in parallel
    parameter OUTPUT_PAR = 1,                 // Number of outputs computed in parallel
    parameter LOG2_OUTPUT_PAR = 1
  ) (
    input                        clk_i,
    input                        rst_i,
    input                        start_config_i,
    input                        data_ready_i,
    output                       rfw_write_enable_o,
    output [RFW_ADDR_SIZE-1:0]   rfw_addr_o,
    output [RFW_ADDR_SIZE-1:0]   rfw_addr_channel_o,
    output                       ready_o,
    output [LOG2_OUTPUT_PAR-1:0] input_buffer_switch_o,
    output [RFW_SIZE-1:0]        write_accumulator_o,
    output                       ignore_accumulator_value_o,
    output [RFW_SIZE-1:0]        enable_max_pool_o,
    output [RFW_SIZE-1:0]        load_max_pool_o,
    output                       fetch_weight_o,
    output                       fetch_input_o
  );

  /*
  * Internal parameters
  */
  localparam INPUT_COUNTER_SIZE = 2;
  localparam LOAD_CYCLE_COUNTER_SIZE = 1;

  localparam FILTERS_ON_PE = NUMBER_PE_PAR/PAR_INPUT_CHANNELS;
  localparam SHIFT_WRITE_SIZE = RFW_SIZE+FILTERS_ON_PE;
  

  // FSM parameters
  localparam NUMBER_STATES = 9;
  localparam SIZE_STAGE_REG = 4;
  localparam RESET = 0, LOAD_WEIGHTS = 1, IDLE = 2, PROCESS_FILTER = 3, 
             NEXT_INPUT_SEQ = 4, PROCESS_FILTER_LOAD_MAX_POOL = 5, 
             PROCESS_FILTER_MAX_POOL = 6, START_NEW_BATCH = 7, START_NEW_BATCH_WITH_LOAD = 8;

  /*
  *	Start internal signals declaration
  */
  // Register file control
  reg                      rfw_write_enable;
  reg  [RFW_ADDR_SIZE-1:0] rfw_addr, rfw_addr_channel;
  reg                      rst_rfw_addr;
  reg                      enable_rfw_addr;
  wire                     next_filter_bank_config;
  reg                      enable_rfw_addr_channel;
  reg                      fetch_weight;
  
  // Input processing control
  reg  [INPUT_COUNTER_SIZE-1:0] input_seq_counter;
  reg                           rst_input_seq_counter, fetch_input_sequence;

  // When computing multiple outputs parallely, we need a counter
  `ifdef L2_USE_OUTPUT_PARALLEL
  reg  [LOG2_OUTPUT_PAR-1:0] output_process_counter;
  reg                        enable_output_process_counter, rst_output_process_counter;
  `endif

  // Accumulators control
  // TODO: improve this, it is too naive
  reg  [RFW_SIZE-1:0] write_accumulator;
  reg                 shift_write_accumulator;

  // Max pooling control
  reg                enable_max_pool, load_max_pool;
  reg                enable_max_pool_delayed, load_max_pool_delayed;
  wire               enable_max_pool_counter;
  reg [1:0]          max_pool_counter;
  reg [RFW_SIZE-1:0] max_pool_load_vector, max_pool_enable_vector;
  
  // FSM registers
  reg [SIZE_STAGE_REG-1:0] current_state, next_state;
  reg config_ready;

  /*
   *  FSM State change
   */
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i == 1'b1)
      current_state <= RESET;
    else
      current_state <= next_state;
  end

  /*
   *  FSM State Definition
   */
  always @(*) begin
    case (current_state)
      RESET: begin
        if (start_config_i)
          next_state <= LOAD_WEIGHTS;
        else
          next_state <= RESET;
      end
      LOAD_WEIGHTS: begin
        if ((rfw_addr == RFW_SIZE-1) && (rfw_addr_channel == RFW_SIZE-1))
          next_state <= IDLE;
        else
          next_state <= LOAD_WEIGHTS;
      end
      IDLE: begin 
        if (data_ready_i)
          next_state <= PROCESS_FILTER;
        else 
          next_state <= IDLE;
      end
      PROCESS_FILTER: begin
        if (rfw_addr == (RFW_SIZE-2*FILTERS_ON_PE))
          next_state <= NEXT_INPUT_SEQ;
        else
          next_state <= PROCESS_FILTER;
      end
      NEXT_INPUT_SEQ: begin
        if (rfw_addr_channel == (NUMBER_INPUT_CHANNELS-FILTERS_ON_PE))
          if (max_pool_counter == 2'd0)
            next_state <= PROCESS_FILTER_LOAD_MAX_POOL;
          else
            next_state <= PROCESS_FILTER_MAX_POOL;
        else
          next_state <= PROCESS_FILTER;
      end
      PROCESS_FILTER_LOAD_MAX_POOL: begin
        if (rfw_addr == (RFW_SIZE-2*FILTERS_ON_PE))
          next_state <= START_NEW_BATCH_WITH_LOAD;
        else
          next_state <= PROCESS_FILTER_LOAD_MAX_POOL;
      end
      PROCESS_FILTER_MAX_POOL: begin
        if (rfw_addr == (RFW_SIZE-2*FILTERS_ON_PE))
          next_state <= START_NEW_BATCH;
        else
          next_state <= PROCESS_FILTER_MAX_POOL;
      end
      START_NEW_BATCH: begin
        next_state <= PROCESS_FILTER;
      end
      START_NEW_BATCH_WITH_LOAD: begin
        next_state <= PROCESS_FILTER;
      end
      default: begin
        next_state <= RESET;
      end
    endcase
  end

  /*
   *  FSM Output Definition
   */
  always @(*) begin
    case (current_state)
      RESET: begin
        // Input processing control
        rst_input_seq_counter         <= 1'b1;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b1;
        enable_rfw_addr_channel       <= 1'b0;
        enable_rfw_addr               <= 1'b0;
        config_ready                  <= 1'b0;

        `ifdef L2_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b1;
        enable_output_process_counter <= 1'b0;
        `endif

        // Accumulator control
        shift_write_accumulator       <= 1'b0;

        // Max pool control
        //enable_max_pool_counter       <= 1'b0;
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      LOAD_WEIGHTS: begin
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b1;
        rfw_write_enable              <= 1'b1;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr_channel       <= 1'b1;
        enable_rfw_addr               <= next_filter_bank_config;
        config_ready                  <= 1'b0;
        
        `ifdef L2_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Accumulator control
        shift_write_accumulator       <= 1'b0;
        
        // Max pool control
        //enable_max_pool_counter       <= 1'b0;
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      IDLE: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr_channel       <= 1'b0;
        enable_rfw_addr               <= 1'b0;
        config_ready                  <= 1'b1;
        
        `ifdef L2_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Accumulator control
        shift_write_accumulator       <= 1'b0;
        
        // Max pool control
        //enable_max_pool_counter       <= 1'b0;
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      PROCESS_FILTER: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        config_ready                  <= 1'b1;

        // Control the operation to compute multiple outputs
        `ifdef L2_USE_OUTPUT_PARALLEL
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b1;
        `endif
        enable_rfw_addr_channel       <= 1'b0;
        enable_rfw_addr               <= 1'b1;

        // Accumulator control
        shift_write_accumulator       <= 1'b1;
        
        // Max pool control
        //enable_max_pool_counter       <= 1'b0;
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      NEXT_INPUT_SEQ: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b1;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr_channel       <= 1'b1;
        enable_rfw_addr               <= 1'b1;
        config_ready                  <= 1'b1;
        
        // Input channels sequence fetch control
        fetch_input_sequence          <= 1'b1;

        `ifdef L2_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Accumulator control
        shift_write_accumulator       <= 1'b1;
        
        // Max pool control
        //enable_max_pool_counter       <= 1'b0;
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      PROCESS_FILTER_LOAD_MAX_POOL: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        config_ready                  <= 1'b1;

        // Control the operation to compute multiple outputs
        `ifdef L2_USE_OUTPUT_PARALLEL
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b1;
        `endif
        enable_rfw_addr_channel       <= 1'b0;
        enable_rfw_addr               <= 1'b1;

        // Accumulator control
        shift_write_accumulator       <= 1'b1;
        
        // Max pool control
        // TODO: check the timing as there is the accumulator delay
        //enable_max_pool_counter       <= 1'b0;
        enable_max_pool               <= 1'b1;
        load_max_pool                 <= 1'b1;
      end
      PROCESS_FILTER_MAX_POOL: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        config_ready                  <= 1'b1;

        // Control the operation to compute multiple outputs
        `ifdef L2_USE_OUTPUT_PARALLEL
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b1;
        `endif
        enable_rfw_addr_channel       <= 1'b0;
        enable_rfw_addr               <= 1'b1;

        // Accumulator control
        shift_write_accumulator       <= 1'b1;
        
        // Max pool control
        // TODO: check the timing as there is the accumulator delay
        //enable_max_pool_counter       <= 1'b0;
        enable_max_pool               <= 1'b1;
        load_max_pool                 <= 1'b0;
      end
      START_NEW_BATCH_WITH_LOAD: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b1;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr_channel       <= 1'b1;
        enable_rfw_addr               <= 1'b1;
        config_ready                  <= 1'b1;
        
        // Input channels sequence fetch control
        fetch_input_sequence          <= 1'b1;

        `ifdef L2_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Accumulator control
        shift_write_accumulator       <= 1'b1;
        
        // Max pool control
        enable_max_pool               <= 1'b1;
        load_max_pool                 <= 1'b1;
      end
      START_NEW_BATCH: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b1;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr_channel       <= 1'b1;
        enable_rfw_addr               <= 1'b1;
        config_ready                  <= 1'b1;
        
        // Input channels sequence fetch control
        fetch_input_sequence          <= 1'b1;

        `ifdef L2_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Accumulator control
        shift_write_accumulator       <= 1'b1;
        
        // Max pool control
        enable_max_pool               <= 1'b1;
        load_max_pool                 <= 1'b0;
      end
      default: begin 
        // Input processing control
        rst_input_seq_counter         <= 1'b0;
        fetch_input_sequence          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr_channel       <= 1'b0;
        enable_rfw_addr               <= 1'b0;
        config_ready                  <= 1'b0;

        // Parallel Output control
        `ifdef L2_USE_OUTPUT_PARALLEL
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Accumulator control
        shift_write_accumulator       <= 1'b0;
        
        // Max pool control
        //enable_max_pool_counter       <= 1'b0;
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
    endcase
  end

  /*
   * Registers 
   */
  // Register file control
  assign next_filter_bank_config = (rfw_addr_channel == RFW_SIZE-1) ? 1'b1 : 1'b0;
  always @(posedge clk_i or posedge rst_rfw_addr) begin
    if (rst_rfw_addr == 1'b1)
      rfw_addr <= {(RFW_ADDR_SIZE){1'b0}};
    else if (enable_rfw_addr) begin
      if (rfw_addr == RFW_SIZE-1)
        rfw_addr <= {(RFW_ADDR_SIZE){1'b0}};
      else
        if (rfw_write_enable)
          rfw_addr <= rfw_addr + 1'b1;
        else
          rfw_addr <= rfw_addr + FILTERS_ON_PE;
    end
  end

  always @(posedge clk_i or posedge rst_rfw_addr) begin
    if (rst_rfw_addr == 1'b1)
      rfw_addr_channel <= {(RFW_ADDR_SIZE){1'b0}};
    else if (enable_rfw_addr_channel) begin
      if (rfw_addr_channel == RFW_SIZE-1)
        rfw_addr_channel <= {(RFW_ADDR_SIZE){1'b0}};
      else
        if (rfw_write_enable)
          rfw_addr_channel <= rfw_addr_channel + 1'b1;
        else
          rfw_addr_channel <= rfw_addr_channel + PAR_INPUT_CHANNELS;
    end
  end

  // Delay lines
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i) begin
      load_max_pool_delayed   <= 1'b0;
      enable_max_pool_delayed <= 1'b0;
    end else begin
      load_max_pool_delayed   <= load_max_pool;
      enable_max_pool_delayed <= enable_max_pool;
    end
  end
  // Input counter control
  always @(posedge clk_i or posedge rst_input_seq_counter) begin
    if (rst_input_seq_counter == 1'b1)
      input_seq_counter <= {(INPUT_COUNTER_SIZE){1'b0}};
    else if (fetch_input_sequence)
      input_seq_counter <= input_seq_counter + 1;
  end

  // Max pool counter
  assign enable_max_pool_counter = ((rfw_addr == {(RFW_SIZE){1'b0}}) && (rfw_addr_channel == {(RFW_SIZE){1'b0}}) && (enable_max_pool_delayed == 1'b1)) ? 1'b1 : 1'b0;
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i == 1'b1)
      max_pool_counter <= {(2){1'b0}};
    else if (enable_max_pool_counter)
      max_pool_counter <= max_pool_counter + 1'b1;
  end

  // Max pool vector generation
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i == 1'b1) begin
      max_pool_enable_vector <= {{(RFW_SIZE-FILTERS_ON_PE){1'b0}}, {(FILTERS_ON_PE){1'b1}}};
    end else if (enable_max_pool_delayed) begin
      //write_accumulator <= {write_accumulator[RFW_SIZE-1:0], write_accumulator[RFW_SIZE+FILTERS_ON_PE-1:RFW_SIZE]};
      //max_pool_enable_vector <= {max_pool_enable_vector[RFW_SIZE-FILTERS_ON_PE-1:0], max_pool_enable_vector[RFW_SIZE-1:RFW_SIZE-FILTERS_ON_PE]};
      max_pool_enable_vector <= {max_pool_enable_vector[RFW_SIZE-FILTERS_ON_PE-1:0], max_pool_enable_vector[RFW_SIZE-1:RFW_SIZE-FILTERS_ON_PE]};
    end
  end
  
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i == 1'b1) begin
      max_pool_load_vector <= {{(RFW_SIZE-FILTERS_ON_PE){1'b0}}, {(FILTERS_ON_PE){1'b1}}};
    end else if ((load_max_pool_delayed == 1'b1) && (max_pool_counter == 2'd0)) begin
      max_pool_load_vector <= {max_pool_load_vector[RFW_SIZE-FILTERS_ON_PE-1:0], max_pool_load_vector[RFW_SIZE-1:RFW_SIZE-FILTERS_ON_PE]};
    end
  end
 
  `ifdef L2_USE_OUTPUT_PARALLEL
  // Output parallel control
  always @(posedge clk_i) begin
    if (rst_output_process_counter == 1'b1)
      output_process_counter <= {(LOG2_OUTPUT_PAR){1'b0}};
    else if (enable_output_process_counter)
      output_process_counter <= output_process_counter + 1'b1;
  end
  `endif

  // Since we have to accumulate the results, we need to select when to write
  // on the accumulation registers
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i == 1'b1)
      write_accumulator <= {{(RFW_SIZE-FILTERS_ON_PE){1'b0}}, {(FILTERS_ON_PE){1'b1}}};
    else if (shift_write_accumulator)
      write_accumulator <= {write_accumulator[RFW_SIZE-FILTERS_ON_PE-1:0], write_accumulator[RFW_SIZE-1:RFW_SIZE-FILTERS_ON_PE]};
  end

  /*
   * Output assignment
   */
  // TODO: check this
  assign fetch_weight_o             = fetch_weight;
  assign fetch_input_o              = fetch_input_sequence;
  assign rfw_write_enable_o         = rfw_write_enable;
  assign rfw_addr_o                 = rfw_addr;
  assign rfw_addr_channel_o         = rfw_addr_channel;
  assign ready_o                    = config_ready;
  assign write_accumulator_o        = write_accumulator & {(RFW_SIZE){shift_write_accumulator}};
  assign ignore_accumulator_value_o = ((rfw_addr_channel == 0) && (config_ready == 1'b1)) ? 1'b1 : 1'b0;
  assign load_max_pool_o            = max_pool_load_vector & {(RFW_SIZE){load_max_pool_delayed}};
  assign enable_max_pool_o          = max_pool_enable_vector & {(RFW_SIZE){enable_max_pool_delayed}};;

  `ifdef L2_USE_OUTPUT_PARALLEL
  assign input_buffer_switch_o = output_process_counter;
  `else
  assign input_buffer_switch_o = 0;
  `endif
endmodule
