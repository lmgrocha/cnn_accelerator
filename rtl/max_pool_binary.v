/* -----------------------------------------
 * File Name :
 *
 * Purpose :
 * Creation Date : 24-10-2019
 * Last Modified : Qui 24 Out 2019 16:21:57 -03
 * Created By : Leandro Rocha
 * -----------------------------------------*/

module max_pool_binary (
    input  clk_i,
    input  rst_i,
    input  enable_i,
    input  load_i,
    input  data_i,
    output data_o
  );

  // Register holder for current max value
  reg  reg_comparator;

  // Holder for comparison result
  wire comparison;

  assign comparison = data_i | reg_comparator;

  // Instatiate the holder register
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i)
      reg_comparator <= 1'b0;
    else if (enable_i) begin
      if (load_i)
        reg_comparator <= data_i;
      else
        reg_comparator <= comparison;
    end
  end

  assign data_o = reg_comparator;
endmodule
