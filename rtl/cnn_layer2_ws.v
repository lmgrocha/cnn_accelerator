
/*********************************************************************************
 *
 * WARNING: The OUTPUT_PAR feature is not fully supported as of now.
 *
 *********************************************************************************
 */

`include "definitions.v"

module cnn_layer_2 #(
    parameter INPUT_CHANNELS = `L2_INPUT_CHANNELS,            // Number of input channels
    parameter LOG2_INPUT_CHANNELS = `L2_LOG2_INPUT_CHANNELS,
    parameter FILTER_SIZE = `L2_FILTER_SIZE,                  // Filter size
    parameter NUMBER_FILTERS = `L2_NUMBER_FILTERS,
    parameter RFW_SIZE = `L2_RFW_SIZE,                        // Weight register file
    parameter RFW_ADDR_SIZE = `L2_RFW_ADDR_SIZE,              // Address for filter loading
    parameter FILTER_PAR = `L2_FILTER_PAR,                    // Filter computation parallelism
    parameter LOG2_FILTER_PAR = `L2_LOG2_FILTER_PAR,
    parameter NUMBER_PE_PAR = `L2_NUMBER_PE_PAR,              // Number of instantiated filters
    parameter LOG2_NUMBER_PE_PAR = `L2_LOG2_NUMBER_PE_PAR,
    parameter PAR_INPUT_CHANNELS = `L2_PAR_INPUT_CHANNELS,    // Number of input channels computed in parallel
    parameter OUTPUT_PAR = 1,                                 // Number of outputs computed in parallel
    parameter LOG2_OUTPUT_PAR = 1
  ) (
    input                               clk_i,
    input                               rst_i,
    input                               start_config_i,
    input                               data_ready_i,
    input  [(FILTER_SIZE+OUTPUT_PAR-1)*PAR_INPUT_CHANNELS-1:0] input_words_i,
    input  [FILTER_SIZE-1:0]            weight_i,
    output                              config_ready_o,
    output                              fetch_inputs_o,
    output                              fetch_weight_o,
    output [RFW_ADDR_SIZE-1:0]          addr_filter_o,
    output [(LOG2_FILTER_PAR+LOG2_INPUT_CHANNELS)-1:0] accumulator_o,
    output                              enable_max_pool_o,
    output                              load_max_pool_o
  );

  /*
   *  SIGNALS DECLARATION
   */
  // Input slice signals
  reg  [(FILTER_SIZE+OUTPUT_PAR-1)-1:0] sliced_inputs [0:PAR_INPUT_CHANNELS-1];

  // Input mux shifters
  wire [FILTER_SIZE-1:0]      shift_data [0:PAR_INPUT_CHANNELS-1];
  wire [LOG2_OUTPUT_PAR-1:0]  shift_control; 

  // Register file signals
  wire [(FILTER_SIZE*PAR_INPUT_CHANNELS)-1:0] rfw_data [0:RFW_SIZE-1];
  wire [(FILTER_SIZE*PAR_INPUT_CHANNELS)-1:0] selected_rfw_data [0:NUMBER_PE_PAR/PAR_INPUT_CHANNELS-1];
  wire [RFW_ADDR_SIZE-1:0] rfw_addr;
  wire [RFW_ADDR_SIZE-1:0] rfw_addr_channel;
  wire                     rfw_write_enable;
  wire [RFW_SIZE-1:0]      rfw_write_enable_vector;

  // Filter signals
  wire [LOG2_FILTER_PAR:0]                          filter_output [0:PAR_INPUT_CHANNELS-1][0:NUMBER_PE_PAR/PAR_INPUT_CHANNELS-1];
  wire [NUMBER_PE_PAR*(LOG2_FILTER_PAR+1)-1:0]      unpacked_filter_output;
  wire [(LOG2_FILTER_PAR+1)+LOG2_NUMBER_PE_PAR-1:0] pe_sum;

  // Accumulators control
  wire [(LOG2_FILTER_PAR+LOG2_INPUT_CHANNELS)-1:0]  accumulation;
  wire [OUTPUT_PAR-1:0]                             write_accumulator;
  `ifdef L2_USE_OUTPUT_PARALLEL
  wire [(LOG2_FILTER_PAR+LOG2_INPUT_CHANNELS)-1:0]  output_accumulators [0:OUTPUT_PAR-1];
  `else
  wire [(LOG2_FILTER_PAR+LOG2_INPUT_CHANNELS)-1:0]  output_accumulators;
  `endif

  // Max pool control
  wire enable_max_pool, load_max_pool;
  
  /*
   *  FSM INSTANTIATION
   */
  fsm_controller_cnn2 #(
    .NUMBER_INPUT_CHANNELS(INPUT_CHANNELS),
    .FILTER_SIZE(FILTER_SIZE),
    .NUMBER_FILTERS(NUMBER_FILTERS),
    .RFW_ADDR_SIZE(RFW_ADDR_SIZE),
    .RFW_SIZE(RFW_SIZE),
    .FILTER_PAR(FILTER_PAR),
    .NUMBER_PE_PAR(NUMBER_PE_PAR),
    .LOG2_NUMBER_PE_PAR(LOG2_NUMBER_PE_PAR),
    .PAR_INPUT_CHANNELS(PAR_INPUT_CHANNELS), 
    .OUTPUT_PAR(OUTPUT_PAR),
    .LOG2_OUTPUT_PAR(LOG2_OUTPUT_PAR)
  ) i_fsm_controller (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .start_config_i(start_config_i),
    .data_ready_i(data_ready_i),
    .rfw_write_enable_o(rfw_write_enable),
    .rfw_addr_o(rfw_addr),
    .rfw_addr_channel_o(rfw_addr_channel),
    .ready_o(config_ready_o),
    .input_buffer_switch_o(shift_control),
    .write_accumulator_o(write_accumulator),
    .enable_max_pool_o(enable_max_pool),
    .load_max_pool_o(load_max_pool),
    .fetch_weight_o(fetch_weight_o)
  );

  /*
   *  INPUT SLICING
   */
  genvar index_slice;
  generate
    for (index_slice = 0; index_slice < PAR_INPUT_CHANNELS; index_slice = index_slice + 1) begin : gen_slice
      always @(*) begin
        sliced_inputs[index_slice] <= input_words_i[(index_slice+1)*(FILTER_PAR+OUTPUT_PAR-1)-1:index_slice*(FILTER_PAR+OUTPUT_PAR-1)];
      end
    end
  endgenerate

  /*
   *  INPUT SELECTION MUXES
   */
  // Use input muxes to handle parallel output computation 
  genvar index_shift;
  genvar index_pe;
  generate
    for (index_pe = 0; index_pe < PAR_INPUT_CHANNELS; index_pe = index_pe + 1) begin : gen_shifter_pe
      if (OUTPUT_PAR == 2) begin
        for (index_shift = 0; index_shift < FILTER_PAR; index_shift = index_shift+1) begin : gen_shifter
          mux_2_inputs #(
            .INPUT_WIDTH(1)
          ) mux_input_shift (
            .data_i(sliced_inputs[index_pe][index_shift+1:index_shift]),
            .selection_i(shift_control),
            .data_o(shift_data[index_pe][index_shift])
          );  
        end
      end else if (OUTPUT_PAR == 3) begin
        for (index_shift = 0; index_shift < FILTER_PAR; index_shift = index_shift+1) begin : gen_shifter
          mux_3_inputs #(
            .INPUT_WIDTH(1)
          ) mux_input_shift (
            .data_i(sliced_inputs[index_pe][index_shift+2:index_shift]),
            .selection_i(shift_control),
            .data_o(shift_data[index_pe][index_shift])
          );  
        end
      end else if (OUTPUT_PAR == 4) begin
        for (index_shift = 0; index_shift < FILTER_PAR; index_shift = index_shift+1) begin : gen_shifter
          mux_4_inputs #(
            .INPUT_WIDTH(1)
          ) mux_input_shift (
            .data_i(sliced_inputs[index_pe][index_shift+3:index_shift]),
            .selection_i(shift_control),
            .data_o(shift_data[index_pe][index_shift])
          );  
        end
      end else begin
        for (index_shift = 0; index_shift < FILTER_PAR; index_shift = index_shift+1) begin : gen_no_shifter
          assign shift_data[index_pe][index_shift] = sliced_inputs[index_pe][index_shift];
        end
      end
    end
  endgenerate

  /*
   * WEIGHT REGISTER FILE INSTANTIATION
   * 
   * Every filter is composed of "subfilters", one for each input channel
   */
  genvar index_rf;
  generate
    for (index_rf = 0; index_rf < RFW_SIZE; index_rf = index_rf + 1) begin : gen_register_file
      register_file #(
        .DEPTH(RFW_SIZE),
        .ADDR_SIZE(RFW_ADDR_SIZE),
        .W(FILTER_SIZE),
        .PARALLEL_READ(PAR_INPUT_CHANNELS)
      ) i_register_file_weights (
        .clk_i(clk_i),
        .rst_i(rst_i),
        .write_i(rfw_write_enable_vector[index_rf]),
        .addr_i(rfw_addr_channel),
        .data_i(weight_i),
        .data_o(rfw_data[index_rf])
        );
    end
  endgenerate

  assign rfw_write_enable_vector = (rfw_write_enable) ? (1 << rfw_addr) : {(RFW_SIZE){1'b0}};

  /*
   *  REGISTER FILE OUTPUT SELECTION
   */
  genvar index_rf_selection;
  generate
    for (index_rf_selection = 0; index_rf_selection < NUMBER_PE_PAR/PAR_INPUT_CHANNELS; index_rf_selection = index_rf_selection + 1) begin : gen_rf_selection
      assign selected_rfw_data[index_rf_selection] = rfw_data[rfw_addr+index_rf_selection];
    end
  endgenerate


  /*
   * FILTER INSTANTIATION
   */
  // To cope with the throughput of CNN1, we need to compute more filters in parallel.
  // If CNN1 computes one output per cycle (i.e, full filter parallelism), we need 8 filters.
  genvar index_par_input;
  genvar index_par_filter;
  generate
    for (index_par_input = 0; index_par_input < PAR_INPUT_CHANNELS; index_par_input = index_par_input + 1) begin : gen_pe_par_input
      for (index_par_filter = 0; index_par_filter < NUMBER_PE_PAR/PAR_INPUT_CHANNELS; index_par_filter = index_par_filter + 1) begin : gen_pe_parfilter
        binary_filter #(
          .FILTER_SIZE(FILTER_SIZE),
          .LOG2_FILTER_SIZE(LOG2_FILTER_PAR)
        ) i_filter (
          .data_i(shift_data[index_par_input]),
          .weights_i(selected_rfw_data[index_par_filter][(index_par_input+1)*FILTER_SIZE-1:index_par_input*FILTER_SIZE]),
          .filter_o(filter_output[index_par_input][index_par_filter])
        );

        //assign unpacked_filter_output[(index_filter+1)*(LOG2_FILTER_PAR+1)-1:index_filter*(LOG2_FILTER_PAR+1)] = filter_output[index_filter];
      end
    end
  endgenerate

  /*
   *  FILTER ADDER TREE
   */
  adder_tree #(
    .W(LOG2_FILTER_PAR+1),
    .NUMBER_INPUTS(NUMBER_PE_PAR),
    .LOG2_NUMBER_INPUTS(LOG2_NUMBER_PE_PAR)
  ) i_adder_tree (
    .data_i(unpacked_filter_output),
    .data_o(pe_sum)
  );

  /*
   * OUTPUT ACCUMULATORS
   */
  `ifdef L2_USE_OUTPUT_PARALLEL
  assign accumulation = output_accumulators[shift_control] + $signed(pe_sum);

  genvar index_register;
  generate
    for (index_register = 0; index_register < OUTPUT_PAR; index_register = index_register + 1) begin : gen_register
      reg_n_bits #(
        .W(LOG2_FILTER_PAR+LOG2_INPUT_CHANNELS)
      ) i_output_register (
        .clk_i(clk_i),
        .rst_i(rst_i),
        .enable_i(write_accumulator[index_register]),
        .data_i(accumulation),
        .data_o(output_accumulators[index_register])
      );
    end
  endgenerate
  `else
  assign accumulation = output_accumulators + $signed(pe_sum);

  reg_n_bits #(
    .W(LOG2_FILTER_PAR+LOG2_INPUT_CHANNELS)
  ) i_output_register (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .enable_i(write_accumulator),
    .data_i(accumulation),
    .data_o(output_accumulators)
  );
  `endif

  /*
   *  OUTPUT ASSIGNMENT
   */
  assign addr_filter_o = rfw_addr;
  `ifdef L2_USE_OUTPUT_PARALLEL
  assign accumulator_o = output_accumulators[shift_control];
  `else
  assign accumulator_o = output_accumulators;
  `endif

endmodule
