
`include "definitions.v"

module fsm_controller_cnn1 #(
    parameter INPUT_PROCESSING_LENGTH = 1000, // Number of input samples
    parameter FILTER_SIZE = 40,               // Filter size
    parameter NUMBER_FILTERS = 32,
    parameter RFW_ADDR_SIZE = 5,              // Address for filter loading
    parameter RFW_SIZE = 32,                  // Weight register file
    parameter FILTER_PAR = 40,                // Filter computation parallelism
    parameter OUTPUT_PAR = 4,                 // Number of outputs computed in parallel
    parameter LOG2_OUTPUT_PAR = 2
  ) (
    input                        clk_i,
    input                        rst_i,
    input                        start_config_i,
    input                        new_window_i,
    output                       rfw_write_enable_o,
    output [RFW_ADDR_SIZE-1:0]   rfw_addr_o,
    output                       ready_o,
    output                       parallel_load_buffer_o,
    output                       shift_load_buffer_o,
    output [LOG2_OUTPUT_PAR-1:0] input_buffer_switch_o,
    output                       enable_max_pool_o,
    output                       load_max_pool_o,
    output                       fetch_ppg_data_o,
    output [1:0]                 fetch_ppg_mode_o,
    output                       fetch_weight_o
  );

  /*
  * Internal parameters
  */
  localparam INPUT_COUNTER_SIZE = 10;
  localparam LOAD_CYCLE_COUNTER_SIZE = 1;

  // FSM parameters
  localparam NUMBER_STATES = 8;
  localparam SIZE_STAGE_REG = 3;
  localparam RESET = 0, IDLE = 1, LOAD_WEIGHTS = 2, PARALLEL_LOAD = 3, PROCESS_FILTER = 4, 
             CHECK_NEXT_STEP = 5, SERIAL_LOAD = 6, WRITE_OUTPUTS = 7;

  // Create the define to determine the FSM behaviour
  //generate
  //  if (OUTPUT_PAR > 1) begin : declare_output_parallel
  //    `define L1_USE_OUTPUT_PARALLEL 1
  //  end
  //endgenerate

  /*
  *	Start internal signals declaration
  */
  // Register file control
  reg                      rfw_write_enable;
  reg  [RFW_ADDR_SIZE-1:0] rfw_addr;
  reg                      rst_rfw_addr, enable_rfw_addr;
  reg                      fetch_weight;
  
  // Input buffer control
  reg                      parallel_load_buffer, shift_load_buffer;

  // Input processing control
  reg  [INPUT_COUNTER_SIZE-1:0] input_counter;
  reg                           rst_input_counter, enable_input_counter;

  // Execute the parallel load over multiple cycles
  `ifdef MULTI_CYCLE_PARALLEL_LOAD
    reg  load_cycle_counter;
    reg  rst_load_cycle_counter, enable_load_cycle_counter;
  `endif
 
  // When computing multiple outputs parallely, we need a counter
  `ifdef L1_USE_OUTPUT_PARALLEL
    reg  [LOG2_OUTPUT_PAR-1:0] output_process_counter;
    reg                        enable_output_process_counter, rst_output_process_counter;
  `endif

  // CNN Filter control
  reg  [RFW_ADDR_SIZE-1:0] filter_counter;
  reg                      rst_filter_counter, enable_filter_counter;
  //reg  [RFW_SIZE:0]        write_output_register;
  //reg                      shift_write_output_register;

  // Max pooling control
  reg enable_max_pool, load_max_pool;
  
  // FSM registers
  reg [SIZE_STAGE_REG-1:0] current_state, next_state;
  reg config_ready;

  /*
   *  FSM State change
   */
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i == 1'b1)
      current_state <= RESET;
    else
      current_state <= next_state;
  end

  /*
   *  FSM State Definition
   */
  always @(*) begin
    case (current_state)
      RESET: begin
        if (start_config_i)
          next_state <= LOAD_WEIGHTS;
        else
          next_state <= RESET;
      end
      LOAD_WEIGHTS: begin
        if (rfw_addr == RFW_SIZE-1)
          next_state <= IDLE;
        else
          next_state <= LOAD_WEIGHTS;
      end
      IDLE: begin 
        if (new_window_i)
          next_state <= PARALLEL_LOAD;
        else 
          next_state <= IDLE;
      end
      PARALLEL_LOAD: begin
        next_state <= PROCESS_FILTER;
      end
      `ifdef L1_USE_OUTPUT_PARALLEL
      PROCESS_FILTER: begin
        if (output_process_counter == OUTPUT_PAR-2)
          next_state <= CHECK_NEXT_STEP;
        else
          next_state <= PROCESS_FILTER;
      end
      CHECK_NEXT_STEP: begin
        if (filter_counter == RFW_SIZE-1)
          if (input_counter >= INPUT_PROCESSING_LENGTH-OUTPUT_PAR)
            next_state <= WRITE_OUTPUTS;
          else
            next_state <= PROCESS_FILTER;
        else
          next_state <= PROCESS_FILTER;
      end
      SERIAL_LOAD: begin
        next_state <= PROCESS_FILTER;
      end
      `else
      PROCESS_FILTER: begin
        if (filter_counter == RFW_SIZE-2)
          next_state <= CHECK_NEXT_STEP;
        else
          next_state <= PROCESS_FILTER;
      end
      CHECK_NEXT_STEP: begin
        if (input_counter >= INPUT_PROCESSING_LENGTH-OUTPUT_PAR)
          next_state <= WRITE_OUTPUTS;
        else
          next_state <= SERIAL_LOAD;
      end
      SERIAL_LOAD: begin
        next_state <= PROCESS_FILTER;
      end
      `endif
      WRITE_OUTPUTS: begin
        next_state <= IDLE;
      end
      default: begin
        next_state <= RESET;
      end
    endcase
  end

  /*
   *  FSM Output Definition
   */
  always @(*) begin
    case (current_state)
      RESET: begin
        // Input counter control
        rst_input_counter             <= 1'b1;
        enable_input_counter          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b1;
        enable_rfw_addr               <= 1'b0;
        config_ready                  <= 1'b0;

        // Input Buffer control
        parallel_load_buffer          <= 1'b0;
        shift_load_buffer             <= 1'b0;
        
        `ifdef L1_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b1;
        enable_output_process_counter <= 1'b0;
        `endif

        // Filter control
        rst_filter_counter            <= 1'b1;
        enable_filter_counter         <= 1'b0;
        //shift_write_output_register   <= 1'b0;

        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      LOAD_WEIGHTS: begin
        // Input counter control
        rst_input_counter             <= 1'b0;
        enable_input_counter          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b1;
        rfw_write_enable              <= 1'b1;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr               <= 1'b1;
        config_ready                  <= 1'b0;

        // Input Buffer control
        parallel_load_buffer          <= 1'b0;
        shift_load_buffer             <= 1'b0;
        
        `ifdef L1_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Filter control
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b0;
        //shift_write_output_register   <= 1'b0;
        
        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      IDLE: begin 
        // Input counter control
        rst_input_counter             <= 1'b0;
        enable_input_counter          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr               <= 1'b0;
        config_ready                  <= 1'b1;

        // Input Buffer control
        parallel_load_buffer          <= 1'b0;
        shift_load_buffer             <= 1'b0;
        
        `ifdef L1_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Filter control
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b0;
        //shift_write_output_register   <= 1'b0;
        
        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      PARALLEL_LOAD: begin
        // Input counter control
        rst_input_counter             <= 1'b0;
        enable_input_counter          <= 1'b1;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr               <= 1'b0;
        config_ready                  <= 1'b1;

        // Input Buffer control
        parallel_load_buffer          <= 1'b1;
        shift_load_buffer             <= 1'b0;
        
        `ifdef L1_USE_OUTPUT_PARALLEL
        // Parallel Output control
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Filter control
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b0;
        //shift_write_output_register   <= 1'b1;
        
        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      PROCESS_FILTER: begin
        // Input counter control
        rst_input_counter             <= 1'b0;
        enable_input_counter          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        config_ready                  <= 1'b1;

        // Input Buffer control
        parallel_load_buffer          <= 1'b0;
        shift_load_buffer             <= 1'b0;
        
        // Control the operation to compute multiple outputs
        `ifdef L1_USE_OUTPUT_PARALLEL
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b1;
        `endif
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b0;
        enable_rfw_addr               <= 1'b0;
        //shift_write_output_register   <= 1'b1;
        
        // Max pool control
        enable_max_pool               <= 1'b1;
        `ifdef L1_USE_OUTPUT_PARALLEL
        if (output_process_counter == 0)
          load_max_pool               <= 1'b1;
        else
          load_max_pool               <= 1'b0;
        `endif
      end
      CHECK_NEXT_STEP: begin
        // Input counter control
        rst_input_counter             <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        config_ready                  <= 1'b1;

        // Input Buffer control
        parallel_load_buffer          <= 1'b0;
        if (filter_counter == RFW_SIZE-1) begin
          shift_load_buffer           <= 1'b1;
        enable_input_counter          <= 1'b1;
        end else begin
          shift_load_buffer           <= 1'b0;
        enable_input_counter          <= 1'b0;
        end
        
        // Control the operation to compute multiple outputs
        `ifdef L1_USE_OUTPUT_PARALLEL
        rst_output_process_counter    <= 1'b1;
        enable_output_process_counter <= 1'b0;
        `endif
        
        // Filter control
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b1;
        enable_rfw_addr               <= 1'b1;
        //shift_write_output_register   <= 1'b1;
        
        // Max pool control
        enable_max_pool               <= 1'b1;
        load_max_pool                 <= 1'b0;
      end
      SERIAL_LOAD: begin
        // Input counter control
        rst_input_counter             <= 1'b0;
        enable_input_counter          <= 1'b1;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr               <= 1'b0;
        config_ready                  <= 1'b1;

        // Input Buffer control
        parallel_load_buffer          <= 1'b0;
        shift_load_buffer             <= 1'b1;
        
        // Parallel Output control
        `ifdef L1_USE_OUTPUT_PARALLEL
        rst_output_process_counter    <= 1'b1;
        enable_output_process_counter <= 1'b0;
        `endif

        // Filter control
        rst_filter_counter            <= 1'b1;
        enable_filter_counter         <= 1'b0;
        //shift_write_output_register   <= 1'b1;
        
        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      WRITE_OUTPUTS: begin
        // Input counter control
        rst_input_counter             <= 1'b0;
        enable_input_counter          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr               <= 1'b0;
        config_ready                  <= 1'b1;

        // Input Buffer control
        parallel_load_buffer          <= 1'b0;
        shift_load_buffer             <= 1'b0;
        
        // Parallel Output control
        `ifdef L1_USE_OUTPUT_PARALLEL
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Filter control
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b0;
        //shift_write_output_register   <= 1'b0;
        
        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
      default: begin
        // Input counter control
        rst_input_counter             <= 1'b0;
        enable_input_counter          <= 1'b0;

        // Register file control
        fetch_weight                  <= 1'b0;
        rfw_write_enable              <= 1'b0;
        rst_rfw_addr                  <= 1'b0;
        enable_rfw_addr               <= 1'b0;
        config_ready                  <= 1'b0;

        // Input Buffer control
        parallel_load_buffer          <= 1'b0;
        shift_load_buffer             <= 1'b0;
        
        // Parallel Output control
        `ifdef L1_USE_OUTPUT_PARALLEL
        rst_output_process_counter    <= 1'b0;
        enable_output_process_counter <= 1'b0;
        `endif

        // Filter control
        rst_filter_counter            <= 1'b0;
        enable_filter_counter         <= 1'b0;
        //shift_write_output_register   <= 1'b0;
        
        // Max pool control
        enable_max_pool               <= 1'b0;
        load_max_pool                 <= 1'b0;
      end
    endcase
  end

  /*
   * Registers 
   */
  // Register file control
  always @(posedge clk_i or posedge rst_rfw_addr) begin
    if (rst_rfw_addr == 1'b1)
      rfw_addr <= {(RFW_ADDR_SIZE){1'b0}};
    else if (enable_rfw_addr) begin
      if (rfw_addr == RFW_SIZE-1)
        rfw_addr <= {(RFW_ADDR_SIZE){1'b0}};
      else
        rfw_addr <= rfw_addr + 1'b1;
    end
  end

  // Input counter control
  always @(posedge clk_i or posedge rst_input_counter) begin
    if (rst_input_counter == 1'b1)
      input_counter <= {(INPUT_COUNTER_SIZE){1'b0}};
    else if (enable_input_counter)
      input_counter <= input_counter + OUTPUT_PAR;
  end

  // Filter control
  always @(posedge clk_i or posedge rst_filter_counter) begin
    if (rst_filter_counter == 1'b1)
      filter_counter <= {(RFW_ADDR_SIZE){1'b0}};
    else if (enable_filter_counter)
      filter_counter <= filter_counter + 1'b1;
  end
 
  `ifdef L1_USE_OUTPUT_PARALLEL
  // Output parallel control
  always @(posedge clk_i) begin
    if (rst_output_process_counter == 1'b1)
      output_process_counter <= {(LOG2_OUTPUT_PAR){1'b0}};
    else if (enable_output_process_counter) begin
      if (output_process_counter == OUTPUT_PAR-1)
        output_process_counter <= {(LOG2_OUTPUT_PAR){1'b0}};
      else
        output_process_counter <= output_process_counter + 1'b1;
    end
  end
  `endif

  // Write enable shift register
  //always @(posedge clk_i or posedge rst_i) begin
  //  if (rst_i == 1'b1)
  //    write_output_register <= {{(RFW_SIZE){1'b0}}, 1'b1};
  //  else if (shift_write_output_register)
  //    write_output_register <= {write_output_register[RFW_SIZE-1:0], write_output_register[RFW_SIZE]};
  //end

  /*
   * Output assignment
   */
  assign fetch_ppg_data_o        = parallel_load_buffer | shift_load_buffer;
  // TODO: check this
  assign fetch_ppg_mode_o        = {parallel_load_buffer, shift_load_buffer};
  assign fetch_weight_o          = fetch_weight;
  assign rfw_write_enable_o      = rfw_write_enable;
  assign rfw_addr_o              = rfw_addr;
  assign ready_o                 = config_ready;
  assign parallel_load_buffer_o  = parallel_load_buffer;
  assign shift_load_buffer_o     = shift_load_buffer;
  assign load_max_pool_o         = load_max_pool;
  assign enable_max_pool_o       = enable_max_pool;

  `ifdef L1_USE_OUTPUT_PARALLEL
  assign input_buffer_switch_o = output_process_counter;
  `else
  assign input_buffer_switch_o = 0;
  `endif
endmodule
