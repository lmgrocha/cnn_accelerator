
module binary_filter #(
    parameter LOG2_FILTER_SIZE = 6,
    parameter [LOG2_FILTER_SIZE-1:0] FILTER_SIZE = 40
  )(
    input  [FILTER_SIZE-1:0]          data_i,
    input  [FILTER_SIZE-1:0]          weights_i,
    output [(LOG2_FILTER_SIZE+1)-1:0] filter_o
  );

  wire [FILTER_SIZE-1:0]      xnor_multiplication;
  reg  [LOG2_FILTER_SIZE-1:0] popcount_value;

  // Binary multiplication is reduced to XNORs
  assign xnor_multiplication = data_i ~^ weights_i;

  /*
   * ADDER TREE
   */
  integer index_bit;
  always @(xnor_multiplication or popcount_value)
  begin
    popcount_value = {(LOG2_FILTER_SIZE){1'b0}};
    for (index_bit = 0; index_bit < FILTER_SIZE; index_bit = index_bit+1)
      popcount_value = popcount_value + xnor_multiplication[index_bit]; // count number of 1s in the xnor result
  end
  
  // Given that the filter is binary, we need to correct the results according to the popcount algorithm
  assign filter_o = (popcount_value << 1) - FILTER_SIZE;
endmodule
