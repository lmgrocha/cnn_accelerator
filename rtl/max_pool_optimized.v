
module max_pool_optimized #(
    parameter W = 5		// register size
  ) (
    input                 clk_i,
    input                 rst_i,
    input                 enable_i,
    input                 load_i,
    input  signed [W-1:0] data_i,
    output signed [W-1:0] data_o
  );

  // Register holder for current max value
  reg  signed [W-1:0] reg_comparator;

  // Holder for comparison result
  wire signed [W-1:0] comparison;

  assign comparison = (data_i > reg_comparator) ? data_i : reg_comparator;

  // Instatiate the holder register
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i)
      reg_comparator <= {(W){1'b0}};
    else if (enable_i) begin
      if (load_i)
        reg_comparator <= data_i;
      else
        reg_comparator <= comparison;
    end
  end

  assign data_o = reg_comparator;
endmodule
