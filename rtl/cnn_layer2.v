
/*********************************************************************************
 *
 * WARNING: The OUTPUT_PAR feature is not fully supported as of now.
 *
 *********************************************************************************
 */

`include "definitions.v"

module cnn_layer_2 #(
    parameter INPUT_CHANNELS = `L2_INPUT_CHANNELS,            // Number of input channels
    parameter LOG2_INPUT_CHANNELS = `L2_LOG2_INPUT_CHANNELS,
    parameter FILTER_SIZE = `L2_FILTER_SIZE,                  // Filter size
    parameter NUMBER_FILTERS = `L2_NUMBER_FILTERS,
    parameter RFW_SIZE = `L2_RFW_SIZE,                        // Weight register file
    parameter RFW_ADDR_SIZE = `L2_RFW_ADDR_SIZE,              // Address for filter loading
    parameter FILTER_PAR = `L2_FILTER_PAR,                    // Filter computation parallelism
    parameter LOG2_FILTER_PAR = `L2_LOG2_FILTER_PAR,
    parameter NUMBER_PE_PAR = `L2_NUMBER_PE_PAR,              // Number of instantiated filters
    parameter LOG2_NUMBER_PE_PAR = `L2_LOG2_NUMBER_PE_PAR,
    parameter PAR_INPUT_CHANNELS = `L2_PAR_INPUT_CHANNELS,    // Number of input channels computed in parallel
    parameter OUTPUT_PAR = 1,                                 // Number of outputs computed in parallel
    parameter LOG2_OUTPUT_PAR = 1,
    localparam ACCUM_SIZE = LOG2_FILTER_PAR+LOG2_INPUT_CHANNELS
  ) (
    input                               clk_i,
    input                               rst_i,
    input                               start_config_i,
    input                               data_ready_i,
    input  [(FILTER_SIZE+OUTPUT_PAR-1)*PAR_INPUT_CHANNELS-1:0] input_words_i,
    input  [FILTER_SIZE-1:0]            weight_i,
    output                              config_ready_o,
    output                              fetch_input_o,
    output                              fetch_weight_o,
    output [RFW_ADDR_SIZE-1:0]          addr_filter_o,
    output [ACCUM_SIZE*(NUMBER_PE_PAR/PAR_INPUT_CHANNELS)-1:0] accumulators_o,
    output [RFW_SIZE-1:0]               enable_max_pool_o,
    output [RFW_SIZE-1:0]               load_max_pool_o
  );

  // Local parameters
  /*
   *  SIGNALS DECLARATION
   */
  // Input slice signals
  reg  [(FILTER_SIZE+OUTPUT_PAR-1)-1:0] sliced_inputs [0:PAR_INPUT_CHANNELS-1];

  // Input mux shifters
  wire [FILTER_SIZE-1:0]      shift_data [0:PAR_INPUT_CHANNELS-1];
  wire [LOG2_OUTPUT_PAR-1:0]  shift_control; 

  // Register file signals
  wire [(FILTER_SIZE*PAR_INPUT_CHANNELS)-1:0] rfw_data [0:RFW_SIZE-1];
  wire [(FILTER_SIZE*PAR_INPUT_CHANNELS)-1:0] selected_rfw_data [0:NUMBER_PE_PAR/PAR_INPUT_CHANNELS-1];
  wire [RFW_ADDR_SIZE-1:0] rfw_addr;
  reg  [RFW_ADDR_SIZE-1:0] rfw_addr_delayed;
  wire [RFW_ADDR_SIZE-1:0] rfw_addr_channel;
  wire                     rfw_write_enable;
  wire [RFW_SIZE-1:0]      rfw_write_enable_vector;

  // Filter signals
  wire [LOG2_FILTER_PAR:0]                 filter_output [0:PAR_INPUT_CHANNELS-1][0:NUMBER_PE_PAR/PAR_INPUT_CHANNELS-1];
  wire [PAR_INPUT_CHANNELS*ACCUM_SIZE-1:0] unpacked_filter_output[0:NUMBER_PE_PAR/PAR_INPUT_CHANNELS-1];

  // Accumulators control
  wire [ACCUM_SIZE-1:0] accumulation[0:NUMBER_PE_PAR/PAR_INPUT_CHANNELS-1];
  wire [RFW_SIZE-1:0]   write_accumulator;
  wire [ACCUM_SIZE-1:0] output_accumulators [0:NUMBER_FILTERS-1];
  wire [ACCUM_SIZE-1:0] selected_output_accumulators [0:NUMBER_PE_PAR/PAR_INPUT_CHANNELS-1];
  wire [ACCUM_SIZE-1:0] selected_output_accumulators_delayed [0:NUMBER_PE_PAR/PAR_INPUT_CHANNELS-1];
  wire                  ignore_accumulator_value;

  // Max pool control
  wire [RFW_SIZE-1:0] enable_max_pool, load_max_pool;
  
  /*
   *  FSM INSTANTIATION
   */
  fsm_controller_cnn2 #(
    .NUMBER_INPUT_CHANNELS(INPUT_CHANNELS),
    .FILTER_SIZE(FILTER_SIZE),
    .NUMBER_FILTERS(NUMBER_FILTERS),
    .RFW_ADDR_SIZE(RFW_ADDR_SIZE),
    .RFW_SIZE(RFW_SIZE),
    .FILTER_PAR(FILTER_PAR),
    .NUMBER_PE_PAR(NUMBER_PE_PAR),
    .LOG2_NUMBER_PE_PAR(LOG2_NUMBER_PE_PAR),
    .PAR_INPUT_CHANNELS(PAR_INPUT_CHANNELS), 
    .OUTPUT_PAR(OUTPUT_PAR),
    .LOG2_OUTPUT_PAR(LOG2_OUTPUT_PAR)
  ) i_fsm_controller (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .start_config_i(start_config_i),
    .data_ready_i(data_ready_i),
    .rfw_write_enable_o(rfw_write_enable),
    .rfw_addr_o(rfw_addr),
    .rfw_addr_channel_o(rfw_addr_channel),
    .ready_o(config_ready_o),
    .input_buffer_switch_o(shift_control),
    .write_accumulator_o(write_accumulator),
    .ignore_accumulator_value_o(ignore_accumulator_value),
    .enable_max_pool_o(enable_max_pool_o),
    .load_max_pool_o(load_max_pool_o),
    .fetch_weight_o(fetch_weight_o),
    .fetch_input_o(fetch_input_o)
  );

  /*
   *  INPUT SLICING
   */
  genvar index_slice;
  generate
    for (index_slice = 0; index_slice < PAR_INPUT_CHANNELS; index_slice = index_slice + 1) begin : gen_slice
      always @(*) begin
        sliced_inputs[index_slice] <= input_words_i[(index_slice+1)*(FILTER_PAR+OUTPUT_PAR-1)-1:index_slice*(FILTER_PAR+OUTPUT_PAR-1)];
      end
    end
  endgenerate

  /*
   *  INPUT SELECTION MUXES
   */
  // Use input muxes to handle parallel output computation 
  genvar index_shift;
  genvar index_pe;
  generate
    for (index_pe = 0; index_pe < PAR_INPUT_CHANNELS; index_pe = index_pe + 1) begin : gen_shifter_pe
      if (OUTPUT_PAR == 2) begin
        for (index_shift = 0; index_shift < FILTER_PAR; index_shift = index_shift+1) begin : gen_shifter
          mux_2_inputs #(
            .INPUT_WIDTH(1)
          ) mux_input_shift (
            .data_i(sliced_inputs[index_pe][index_shift+1:index_shift]),
            .selection_i(shift_control),
            .data_o(shift_data[index_pe][index_shift])
          );  
        end
      end else if (OUTPUT_PAR == 3) begin
        for (index_shift = 0; index_shift < FILTER_PAR; index_shift = index_shift+1) begin : gen_shifter
          mux_3_inputs #(
            .INPUT_WIDTH(1)
          ) mux_input_shift (
            .data_i(sliced_inputs[index_pe][index_shift+2:index_shift]),
            .selection_i(shift_control),
            .data_o(shift_data[index_pe][index_shift])
          );  
        end
      end else if (OUTPUT_PAR == 4) begin
        for (index_shift = 0; index_shift < FILTER_PAR; index_shift = index_shift+1) begin : gen_shifter
          mux_4_inputs #(
            .INPUT_WIDTH(1)
          ) mux_input_shift (
            .data_i(sliced_inputs[index_pe][index_shift+3:index_shift]),
            .selection_i(shift_control),
            .data_o(shift_data[index_pe][index_shift])
          );  
        end
      end else begin
        for (index_shift = 0; index_shift < FILTER_PAR; index_shift = index_shift+1) begin : gen_no_shifter
          assign shift_data[index_pe][index_shift] = sliced_inputs[index_pe][index_shift];
        end
      end
    end
  endgenerate

  /*
   * WEIGHT REGISTER FILE INSTANTIATION
   * 
   * Every filter is composed of "subfilters", one for each input channel
   */
  genvar index_rf;
  generate
    for (index_rf = 0; index_rf < RFW_SIZE; index_rf = index_rf + 1) begin : gen_register_file
      register_file #(
        .DEPTH(RFW_SIZE),
        .ADDR_SIZE(RFW_ADDR_SIZE),
        .W(FILTER_SIZE),
        .PARALLEL_READ(PAR_INPUT_CHANNELS)
      ) i_register_file_weights (
        .clk_i(clk_i),
        .rst_i(rst_i),
        .write_i(rfw_write_enable_vector[index_rf]),
        .addr_i(rfw_addr_channel),
        .data_i(weight_i),
        .data_o(rfw_data[index_rf])
        );
    end
  endgenerate

  assign rfw_write_enable_vector = (rfw_write_enable) ? (1 << rfw_addr) : {(RFW_SIZE){1'b0}};

  /*
   *  REGISTER FILE OUTPUT SELECTION
   */
  genvar index_rf_selection;
  generate
    for (index_rf_selection = 0; index_rf_selection < NUMBER_PE_PAR/PAR_INPUT_CHANNELS; index_rf_selection = index_rf_selection + 1) begin : gen_rf_selection
      assign selected_rfw_data[index_rf_selection] = rfw_data[rfw_addr+index_rf_selection];
    end
  endgenerate

  /*
   * FILTER INSTANTIATION
   */
  // To cope with the throughput of CNN1, we need to compute more filters in parallel.
  // If CNN1 computes one output per cycle (i.e, full filter parallelism), we need 8 filters.
  genvar index_par_input;
  genvar index_par_filter;
  generate
    for (index_par_input = 0; index_par_input < PAR_INPUT_CHANNELS; index_par_input = index_par_input + 1) begin : gen_pe_par_input
      for (index_par_filter = 0; index_par_filter < NUMBER_PE_PAR/PAR_INPUT_CHANNELS; index_par_filter = index_par_filter + 1) begin : gen_pe_parfilter
        binary_filter #(
          .FILTER_SIZE(FILTER_SIZE),
          .LOG2_FILTER_SIZE(LOG2_FILTER_PAR)
        ) i_filter (
          .data_i(shift_data[index_par_input]),
          .weights_i(selected_rfw_data[index_par_filter][(index_par_input+1)*FILTER_SIZE-1:index_par_input*FILTER_SIZE]),
          .filter_o(filter_output[index_par_input][index_par_filter])
        );

        assign unpacked_filter_output[index_par_filter][(index_par_input+1)*ACCUM_SIZE-1:index_par_input*ACCUM_SIZE] = {{(ACCUM_SIZE-LOG2_FILTER_PAR){filter_output[index_par_input][index_par_filter][LOG2_FILTER_PAR]}}, filter_output[index_par_input][index_par_filter]};
      end
    end
  endgenerate
  
  /*
   *  ACCUMULATION LOGIC
   */
  genvar index_acc;
  generate
    for (index_acc = 0; index_acc < NUMBER_PE_PAR/PAR_INPUT_CHANNELS; index_acc = index_acc + 1) begin : gen_acc_logic
      accum_tree #(
        .W(ACCUM_SIZE),
        .NUMBER_INPUTS(PAR_INPUT_CHANNELS+1),
        .OUTPUT_SIZE(ACCUM_SIZE)
      ) i_accum_tree (
        .data_i({unpacked_filter_output[index_acc], selected_output_accumulators[index_acc]}),
        .data_o(accumulation[index_acc])
      );
    end
  endgenerate

  /*
   *  ACCUMULATOR REGISTER SELECTION
   */
  always @(posedge clk_i) begin
    rfw_addr_delayed <= rfw_addr;
  end

  genvar index_acc_selection;
  generate
    for (index_acc_selection = 0; index_acc_selection < NUMBER_PE_PAR/PAR_INPUT_CHANNELS; index_acc_selection = index_acc_selection + 1) begin : gen_acc_selection
      // When new windows are considered, we have to ignore the previous accumulator values. Instead of a full reset, we perform a data gating
      assign selected_output_accumulators[index_acc_selection] = output_accumulators[rfw_addr+index_acc_selection] & {(ACCUM_SIZE){~ignore_accumulator_value}};
      assign selected_output_accumulators_delayed[index_acc_selection] = output_accumulators[rfw_addr_delayed+index_acc_selection];
    end
  endgenerate

  /*
   * OUTPUT ACCUMULATORS
   */
  genvar index_accumulator;
  generate
    for (index_accumulator = 0; index_accumulator < RFW_SIZE; index_accumulator = index_accumulator + 1) begin : gen_accumulator
      reg_n_bits #(
        .W(ACCUM_SIZE)
      ) i_output_register (
        .clk_i(clk_i),
        .rst_i(rst_i),
        .enable_i(write_accumulator[index_accumulator]),
        .data_i(accumulation[index_accumulator % (NUMBER_PE_PAR/PAR_INPUT_CHANNELS)]),
        .data_o(output_accumulators[index_accumulator])
      );
    end
  endgenerate

  /*
   *  OUTPUT ASSIGNMENT
   */
  // Apply data gating
  assign addr_filter_o = rfw_addr & {(RFW_ADDR_SIZE){config_ready_o}};
 
  genvar index_output_acc;
  generate
    for (index_output_acc = 0; index_output_acc < NUMBER_PE_PAR/PAR_INPUT_CHANNELS; index_output_acc = index_output_acc + 1) begin : gen_acc_output
      assign accumulators_o[(index_output_acc+1)*ACCUM_SIZE-1:index_output_acc*ACCUM_SIZE] = selected_output_accumulators_delayed[index_output_acc];
    end
  endgenerate
endmodule
