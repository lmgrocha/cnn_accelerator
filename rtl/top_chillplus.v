
`include "definitions.v"

module top_chillplus (
    input                               clk_i,
    input                               rst_i,
    input                               start_config_i,
    input                               new_window_i,
    input  [`PPG_INTERFACE_WIDTH-1:0]   ppg_data_i,
    input  [`L1_FILTER_SIZE-1:0]        weight_i,
    input  [`BN_THRESHOLD_WIDTH-1:0]    threshold_value_i,
    output                              fetch_ppg_data_o,
    output [1:0]                        ppg_read_mode_o,
    output                              weight_source_o,
    output                              fetch_weight_o,
    output                              fetch_threshold_o,
    output [`BN_RFW_ADDR_SIZE-1:0]      threshold_addr_o,
    output [`L1_RFW_ADDR_SIZE-1:0]      cnn1_filter_addr_o,
    output [`L2_FILTER_SIZE-1:0]        memory_buffer_o,
    output                              done_config_o
  );
  
  // Signals for CNN1
  localparam L1_ACCUM_SIZE = (`PPG_WORD_WIDTH+1)+`L1_LOG2_FILTER_PAR;
  wire                          cnn1_start_config;
  wire                          cnn1_config_ready;
  wire                          cnn1_fetch_ppg;
  wire [1:0]                    cnn1_ppg_read_mode;
  wire                          cnn1_fetch_weight;
  wire [`L1_RFW_ADDR_SIZE-1:0]  cnn1_addr_filter;
  wire [L1_ACCUM_SIZE-1:0]      cnn1_accumulator;
  wire                          cnn1_enable_max_pool;
  wire                          cnn1_load_max_pool;

  // CNN1 pipeline registers
  reg  [`L1_RFW_ADDR_SIZE-1:0]  cnn1_addr_filter_pipeline;
  reg  [L1_ACCUM_SIZE-1:0]      cnn1_accumulator_pipeline;
  reg                           cnn1_enable_max_pool_pipeline;
  reg                           cnn1_load_max_pool_pipeline;

  // Max Pool 1
  wire mp1_max_pooled;
  
  // Binarizer + BN signals 1
  wire                         bn1_load_threshold;
  wire [`BN_RFW_ADDR_SIZE-1:0] bn1_threshold_address;
  wire                         bn1_binary_output;

  // CNN2 signals
  localparam L2_ACCUM_SIZE = `L2_LOG2_FILTER_PAR+`L2_LOG2_INPUT_CHANNELS;
  localparam L2_INPUT_SIZE = (`L2_FILTER_SIZE+`L2_OUTPUT_PAR-1)*`L2_PAR_INPUT_CHANNELS;
  localparam L2_PAR_ACC = L2_ACCUM_SIZE * `L2_PAR_ACC_PROC;
  localparam L2_PAR_ACC_PROC = `L2_PAR_ACC_PROC;
  wire                         cnn2_fetch_input;
  wire                         cnn2_start_config;
  wire                         cnn2_config_ready;
  wire                         cnn2_fetch_weight;
  wire [`L2_RFW_ADDR_SIZE-1:0] cnn2_addr_filter;
  reg  [`L2_RFW_ADDR_SIZE-1:0] cnn2_addr_filter_delayed;
  wire [L2_PAR_ACC-1:0]        cnn2_accumulators;
  wire [`L2_RFW_SIZE-1:0]      cnn2_enable_max_pool;
  wire [`L2_RFW_SIZE-1:0]      cnn2_load_max_pool;

  // Memory buffer
  wire                     membuff_rst;
  reg [3:0]                membuff_write;
  wire                     membuff_data_ready;
  wire [L2_INPUT_SIZE-1:0] membuff_data_o;
  
  // Binarizer + BN signals 2
  wire                         bn2_load_threshold;
  wire [`BN_RFW_ADDR_SIZE-1:0] bn2_threshold_address;
  wire [`L2_PAR_ACC_PROC-1:0]  bn2_binary_output;

  // Max Pool 2
  wire [`L2_NUMBER_FILTERS-1:0] mp2_max_pooled;

  // Common signals
  wire                         bn_threshold_address_source;
  wire [`BN_RFW_ADDR_SIZE-1:0] bn_threshold_address_config;

  /*
   * Top Controller
   */
  fsm_top_controller i_fsm_top_controller (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .start_config_i(start_config_i),
    .weight_source_o(weight_source_o),
    .ready_cnn1_i(cnn1_config_ready),
    .ready_cnn2_i(cnn2_config_ready),
    .config_cnn1_o(cnn1_start_config),
    .config_cnn2_o(cnn2_start_config),
    .load_bn1_threshold_o(bn1_load_threshold),
    .load_bn2_threshold_o(bn2_load_threshold),
    .bn_addr_source_o(bn_threshold_address_source),
    .bn_addr_o(bn_threshold_address_config),
    .done_config_o(done_config_o)
    );
  
  assign bn1_threshold_address = (bn_threshold_address_source) ? cnn1_addr_filter_pipeline : bn_threshold_address_config;
  assign bn2_threshold_address = (bn_threshold_address_source) ? cnn2_addr_filter_delayed : bn_threshold_address_config & {(`BN_RFW_ADDR_SIZE){~bn1_load_threshold}};

  /*
   * First Convolutional Layer
   */
  cnn_layer_1 #(
    .INPUT_WIDTH(`PPG_WORD_WIDTH),
    .FILTER_SIZE(`L1_FILTER_SIZE),
    .NUMBER_FILTERS(`L1_NUMBER_FILTERS),
    .RFW_SIZE(`L1_RFW_SIZE),
    .RFW_ADDR_SIZE(`L1_RFW_ADDR_SIZE),
    .FILTER_PAR(`L1_FILTER_PAR),
    .OUTPUT_PAR(`L1_OUTPUT_PAR)
  ) i_cnn_layer1 (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .start_config_i(cnn1_start_config),
    .new_window_i(new_window_i),
    .ppg_data_i(ppg_data_i),
    .weight_i(weight_i),
    .config_ready_o(cnn1_config_ready),
    .fetch_ppg_data_o(cnn1_fetch_ppg),
    .ppg_read_mode_o(cnn1_ppg_read_mode),
    .fetch_weight_o(cnn1_fetch_weight),
    .addr_filter_o(cnn1_addr_filter),
    .accumulator_o(cnn1_accumulator),
    .enable_max_pool_o(cnn1_enable_max_pool),
    .load_max_pool_o(cnn1_load_max_pool)
  );
  
  // Pipeline registers for shorter critical path
  always @(posedge clk_i) begin
    cnn1_addr_filter_pipeline     <= cnn1_addr_filter;
    cnn1_accumulator_pipeline     <= cnn1_accumulator;
    cnn1_enable_max_pool_pipeline <= cnn1_enable_max_pool;
    cnn1_load_max_pool_pipeline   <= cnn1_load_max_pool;
  end

  /*
   * Batch norm + binarization module
   */
  // TODO: maybe insert data gating on the input
  binarizer #(
    .THRESHOLD_WIDTH(`BN_THRESHOLD_WIDTH),
    .INPUT_WIDTH(L1_ACCUM_SIZE),
    .NUMBER_FILTERS(`L1_NUMBER_FILTERS),
    .LOG2_NUMBER_FILTERS(`L1_RFW_ADDR_SIZE)  
  ) i_binarizer_cnn1 (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .load_thresh_i(bn1_load_threshold),
    .thres_value_i(threshold_value_i),
    .thres_addr_i(bn1_threshold_address),
    .input_value_i(cnn1_accumulator_pipeline),
    .binary_o(bn1_binary_output)
  );

  /*
   * Max pooling layer
   */
  max_pool_binary i_max_pool_layer1 (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .enable_i(cnn1_enable_max_pool_pipeline),
    .load_i(cnn1_load_max_pool_pipeline),
    .data_i(bn1_binary_output),
    .data_o(mp1_max_pooled)
  );
  
  /*
   * Memory for CNN1 output feature maps
   */
  memory_buffer #(
    .WIDTH(`L1_NUMBER_FILTERS),
    .DEPTH(`L2_FILTER_SIZE),
    .PARALLEL_EXT(`L2_OUTPUT_PAR-1),
    .PARALLEL_CHANNELS(`L2_PAR_INPUT_CHANNELS)
  ) i_memory_buffer (
    .clk_i(clk_i),
    .rst_i(membuff_rst),
    .write_buffer_i(membuff_write[3]),
    .data_i(mp1_max_pooled),
    .fetch_memory_data_i(cnn2_fetch_input),
    .data_ready_o(membuff_data_ready),
    .data_o(membuff_data_o)
  );

  // The memory buffer write signal is a delayed version od the max pool load
  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i)
      membuff_write <= 4'b0000;
    else
      membuff_write <= {membuff_write[2:0], cnn1_load_max_pool_pipeline};
  end

  /*
   * Second Convolutional Layer
   */
  cnn_layer_2 #(
    .INPUT_CHANNELS(`L2_INPUT_CHANNELS),
    .LOG2_INPUT_CHANNELS(`L2_LOG2_INPUT_CHANNELS),
    .FILTER_SIZE(`L2_FILTER_SIZE),
    .NUMBER_FILTERS(`L2_NUMBER_FILTERS),
    .RFW_SIZE(`L2_RFW_SIZE),
    .RFW_ADDR_SIZE(`L2_RFW_ADDR_SIZE),
    .FILTER_PAR(`L2_FILTER_PAR),
    .LOG2_FILTER_PAR(`L2_LOG2_FILTER_PAR),
    .NUMBER_PE_PAR(`L2_NUMBER_PE_PAR),
    .LOG2_NUMBER_PE_PAR(`L2_LOG2_NUMBER_PE_PAR),
    .PAR_INPUT_CHANNELS(`L2_PAR_INPUT_CHANNELS),
    .OUTPUT_PAR(`L2_OUTPUT_PAR),
    .LOG2_OUTPUT_PAR(`L2_LOG2_OUTPUT_PAR)
  ) i_cnn_layer_2 (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .start_config_i(cnn2_start_config),
    .data_ready_i(membuff_data_ready),
    .input_words_i(membuff_data_o),
    .weight_i(weight_i),
    .config_ready_o(cnn2_config_ready),
    .fetch_input_o(cnn2_fetch_input),
    .fetch_weight_o(cnn2_fetch_weight),
    .addr_filter_o(cnn2_addr_filter),
    .accumulators_o(cnn2_accumulators),
    .enable_max_pool_o(cnn2_enable_max_pool),
    .load_max_pool_o(cnn2_load_max_pool)
  );

  // Delay the filter address for BN address
  always @(posedge clk_i) begin
    cnn2_addr_filter_delayed <= cnn2_addr_filter;
  end

  /*
   * Batch norm + binarization module
   */
  binarizer #(
    .THRESHOLD_WIDTH(`BN_THRESHOLD_WIDTH),
    .INPUT_WIDTH(L2_ACCUM_SIZE),
    .NUMBER_FILTERS(`L2_NUMBER_FILTERS),
    .LOG2_NUMBER_FILTERS(`L2_RFW_ADDR_SIZE),
    .PARALLEL_COMPARE(`L2_PAR_ACC_PROC)
  ) i_binarizer_cnn2 (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .load_thresh_i(bn2_load_threshold),
    .thres_value_i(threshold_value_i),
    .thres_addr_i(bn2_threshold_address),
    .input_value_i(cnn2_accumulators),
    .binary_o(bn2_binary_output)
  );

  /*
   * Max pooling layer
   */
  genvar index_maxpool;
  generate
    for (index_maxpool = 0; index_maxpool < `L2_NUMBER_FILTERS; index_maxpool = index_maxpool + 1) begin : gen_maxpool2
      max_pool_binary i_max_pool_layer2 (
        .clk_i(clk_i),
        .rst_i(rst_i),
        .enable_i(cnn2_enable_max_pool[index_maxpool]),
        .load_i(cnn2_load_max_pool[index_maxpool]),
        .data_i(bn2_binary_output[index_maxpool % L2_PAR_ACC_PROC]),
        .data_o(mp2_max_pooled[index_maxpool])
      );
    end
  endgenerate
  /*
   *  Internal signals assignment
   */
  // TODO: maybe there it requires other reset steps
  assign membuff_rst      = rst_i;

  // TODO: the testbench has a specific counter to load the weight memory.
  // In the furture, we have to integrate it to the top level design or
  // assume a contigous memory for all weights and create a single counter 
  // to load them all
  /*
   * Output Assignment
   */
  assign fetch_ppg_data_o   = cnn1_fetch_ppg;
  assign fetch_weight_o     = cnn1_fetch_weight | cnn2_fetch_weight;
  assign ppg_read_mode_o    = cnn1_ppg_read_mode;
  assign cnn1_filter_addr_o = cnn1_addr_filter;
  assign fetch_threshold_o  = bn1_load_threshold | bn2_load_threshold;
  assign threshold_addr_o   = bn1_threshold_address;
  assign memory_buffer_o    = membuff_data_o;
endmodule
