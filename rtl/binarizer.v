
`include "definitions.v"

module binarizer #(
    parameter THRESHOLD_WIDTH     = `BN_THRESHOLD_WIDTH,
    parameter INPUT_WIDTH         = `PPG_WORD_WIDTH+`L1_FILTER_PAR,
    parameter NUMBER_FILTERS      = `L1_NUMBER_FILTERS,
    parameter LOG2_NUMBER_FILTERS = `L1_RFW_ADDR_SIZE,
    parameter PARALLEL_COMPARE    = 1
  ) (
    input                                               clk_i,
    input                                               rst_i,
    input                                               load_thresh_i,
    input  signed [THRESHOLD_WIDTH-1:0]                 thres_value_i,
    input         [LOG2_NUMBER_FILTERS-1:0]             thres_addr_i,
    input  signed [(INPUT_WIDTH*PARALLEL_COMPARE)-1:0]  input_value_i,
    output        [PARALLEL_COMPARE-1:0]                binary_o
  );

  wire signed [(THRESHOLD_WIDTH*PARALLEL_COMPARE)-1:0] current_threshold;
  /*
   * THRESHOLD REGISTER FILE
   */
  register_file #(
    .DEPTH(NUMBER_FILTERS),
    .ADDR_SIZE(LOG2_NUMBER_FILTERS),
    .W(THRESHOLD_WIDTH),
    .PARALLEL_READ(PARALLEL_COMPARE)
  ) i_register_file_threshold (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .write_i(load_thresh_i),
    .addr_i(thres_addr_i),
    .data_i(thres_value_i),
    .data_o(current_threshold)
  );

  // Compare the output and binarize it
  genvar index_compare;
  generate
    if (PARALLEL_COMPARE > 1)
      for (index_compare = 0; index_compare < PARALLEL_COMPARE; index_compare = index_compare + 1) begin : gen_comparator
        assign binary_o[index_compare] = ($signed(input_value_i[(index_compare+1)*INPUT_WIDTH-1:index_compare*INPUT_WIDTH]) >= $signed(current_threshold[(index_compare+1)*THRESHOLD_WIDTH-1:index_compare*THRESHOLD_WIDTH])) ? 1'b1 : 1'b0;
      end
    else
      assign binary_o = (input_value_i >= current_threshold) ? 1'b1 : 1'b0;
  endgenerate
endmodule
