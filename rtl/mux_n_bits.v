
module mux_n_bits #(
    parameter M = 2,  // number of input words
    parameter S = 1,  // number of select bits: S = ceil(log2(M))
    parameter W = 1   // width of one word
  ) (
    input  [M*W-1:0] data_i,
    input  [S-1:0]   sel_i,
    output [W-1:0]   data_o
  );

  localparam NUMBER_OPTIONS = 2**S;

  wire [W-1:0] data[0:NUMBER_OPTIONS-1];
  genvar i;

  generate
    for (i = 0; i < M; i = i+1) begin : data_split
      assign data[i] = data_i[(i+1)*W-1:i*W];
    end
    for (i = M; i < NUMBER_OPTIONS; i = i+1) begin : dont_cares
      assign data[i] = {W{1'bx}};
    end
  endgenerate

  assign data_o = data[sel_i];
endmodule
