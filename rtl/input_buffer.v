
module input_buffer #(
    parameter W = 5,          // Buffer width
    parameter DEPTH = 5,     // Buffer depth
    parameter SHIFT_SIZE = 3
  ) (
    input                               clk_i,
    input                               rst_i,
    input                               parallel_load_i,
    input                               shift_load_i,
    input  [W*(DEPTH+SHIFT_SIZE-1)-1:0] parallel_data_i,
    input  [SHIFT_SIZE*W-1:0]           serial_data_i,
    output [W*(DEPTH+SHIFT_SIZE-1)-1:0] data_o
  );

  // Buffer
  reg  [W*(DEPTH+SHIFT_SIZE-1)-1:0] buffer_data;

  always @(posedge clk_i or posedge rst_i) begin
    if (rst_i) begin
      buffer_data <= {(W*(DEPTH+SHIFT_SIZE-1)){1'b0}};
    end else begin
      if (parallel_load_i) begin
        buffer_data <= parallel_data_i;
      end else if (shift_load_i) begin
        buffer_data <= {serial_data_i, buffer_data[W*(DEPTH+SHIFT_SIZE-1)-1:W*SHIFT_SIZE]};
      end
    end
  end
  
  assign data_o = buffer_data;

endmodule
