
module register_file #(
    parameter DEPTH = 32,       // number of registers
    parameter ADDR_SIZE = 5,    // register address
    parameter W = 40,           // Register size
    parameter PARALLEL_READ = 1 // Number of parallel outputs (sequential)
  ) (
    input                           clk_i,
    input                           rst_i,
    input                           write_i,
    input  [ADDR_SIZE-1:0]          addr_i,
    input  [W-1:0]                  data_i,
    output [(PARALLEL_READ*W)-1:0]  data_o
  );

  // Register file
  reg  [W-1:0] register_file [DEPTH-1:0];
  
  // Data selection
  reg  [PARALLEL_READ*W-1:0] register_selection;

  always @(posedge clk_i) begin
    if (write_i)
      register_file[addr_i] <= data_i;
  end
 
  genvar index_output;
  generate
    for (index_output = 0; index_output < PARALLEL_READ; index_output = index_output + 1) begin : gen_output
      always @(addr_i) begin
        if (addr_i > DEPTH-PARALLEL_READ) begin
          register_selection[(index_output+1)*W-1:index_output*W] <= register_file[DEPTH-1];
        end else begin
          register_selection[(index_output+1)*W-1:index_output*W] <= register_file[addr_i+index_output];
        end
      end 
    end
  endgenerate

  assign data_o = register_selection;
endmodule
