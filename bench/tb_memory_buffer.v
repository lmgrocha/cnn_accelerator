
module tb_memory_buffer ();
  localparam DEPTH = 5;
  localparam WIDTH = 4;
  
  // Wires and regs
  reg  clk, rst, write_buffer, data_i, fetch_memory_data;
  wire data_ready;
  wire [DEPTH-1:0] data_o;
  
  // DUT instantiation
  memory_buffer #(
    .WIDTH(WIDTH), 
    .DEPTH(DEPTH)
  ) dut (
    .clk_i(clk),
    .rst_i(rst),
    .write_buffer_i(write_buffer),
    .data_i(data_i),
    .fetch_memory_data_i(fetch_memory_data),
    .data_ready_o(data_ready),
    .data_o(data_o)
  );

  always begin
    #5 clk = ~clk;
  end

  initial begin
    clk = 1;
    rst = 1;
    write_buffer = 0;
    data_i = 0;
    fetch_memory_data = 0;
    @(posedge clk);
    @(posedge clk);
    rst = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 0;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 0;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 0;
    write_buffer = 1;
    fetch_memory_data = 1;
    @(posedge clk);
    write_buffer = 0;
    fetch_memory_data = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 0;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 0;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 0;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 0;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 0;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    fetch_memory_data = 1;
    @(posedge clk);
    fetch_memory_data = 0;
    @(posedge clk);
    data_i = 0;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 1;
    @(posedge clk);
    write_buffer = 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    data_i = 1;
    write_buffer = 0;
  end
endmodule
