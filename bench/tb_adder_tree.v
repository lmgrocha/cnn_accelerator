`timescale 1ns/1ps

module tb_adder_tree ();
  localparam W = 5;
  localparam NI = 5;

  // Wires
  reg  [W*NI-1:0] adder_tree_i;
  wire [W+NI-1:0] adder_tree_o;

  // DUT Instantiation
  adder_tree #(
    .W(),
    .NI()
  ) dut (
    .data_i(adder_tree_i),
    .data_o(adder_tree_o)
  );

  // Stimuli
  initial begin
    adder_tree_i[W-1:0] <= 5'd0;
    adder_tree_i[2*W-1:W] <= 5'd0;
    adder_tree_i[3*W-1:2*W] <= 5'd0;
    adder_tree_i[4*W-1:3*W] <= 5'd0;
    adder_tree_i[5*W-1:4*W] <= 5'd0;

    #10 adder_tree_i[W-1:0] <= 5'd1;
    adder_tree_i[2*W-1:W] <= 5'd1;
    adder_tree_i[3*W-1:2*W] <= 5'd1;
    adder_tree_i[4*W-1:3*W] <= 5'd1;
    adder_tree_i[5*W-1:4*W] <= 5'd1;

    #10 adder_tree_i[W-1:0] <= -5'd1;
    adder_tree_i[2*W-1:W] <= -5'd1;
    adder_tree_i[3*W-1:2*W] <= -5'd1;
    adder_tree_i[4*W-1:3*W] <= -5'd1;
    adder_tree_i[5*W-1:4*W] <= -5'd1;

    #10 adder_tree_i[W-1:0] <= -5'd1;
    adder_tree_i[2*W-1:W] <= 5'd1;
    adder_tree_i[3*W-1:2*W] <= -5'd1;
    adder_tree_i[4*W-1:3*W] <= 5'd1;
    adder_tree_i[5*W-1:4*W] <= -5'd1;

    #10 adder_tree_i[W-1:0] <= 5'd10;
    adder_tree_i[2*W-1:W] <= 5'd5;
    adder_tree_i[3*W-1:2*W] <= -5'd7;
    adder_tree_i[4*W-1:3*W] <= 5'd14;
    adder_tree_i[5*W-1:4*W] <= -5'd3;
  end
endmodule
