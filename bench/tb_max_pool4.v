`timescale 1ns/1ps

module tb_max_pool4 ();
  localparam W = 5;

  // Wires
  reg          clk_i, rst_i;
  reg  [W-1:0] input_comp;
  reg          load_input;
  wire [W-1:0] max_pool_o;

  // DUT Instantiation
  max_pool4 #(
    .W(W)
  ) dut (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .load_i(load_input),
    .data_i(input_comp),
    .data_o(max_pool_o)
  );

  always begin
    #5 clk_i = ~clk_i;
  end

  // Stimuli
  initial begin
    clk_i = 1;
    rst_i = 1;
    load_input = 0;
    input_comp = 0;

    #30 rst_i = 0;
    #10 input_comp = 0;
    load_input = 1;
    #10 input_comp = 1;
    load_input = 1;
    #10 input_comp = 2;
    load_input = 1;
    #10 input_comp = 3;
    load_input = 1;
    #10 input_comp = 4;
    load_input = 1;
    #10 input_comp = 5;
    load_input = 1;
    #10 input_comp = 0;
    load_input = 1;
    #10 input_comp = -1;
    load_input = 1;
    #10 input_comp = -2;
    load_input = 1;
    #10 input_comp = -3;
    load_input = 1;
    #10 input_comp = -4;
    load_input = 1;
    #10 input_comp = -5;
    load_input = 1;
    #10 input_comp = 0;
    load_input = 0;
  end
endmodule
