
module tb_register_file_weights ();
  localparam DEPTH = 5;
  localparam ADDR_SIZE = 3;
  localparam W = 8;
  localparam PARALLEL_READ = 2;

  // Wires
  reg                  clk_i, rst_i, write_i;
  reg  [ADDR_SIZE-1:0] addr_i;
  reg  [W-1:0]         weight_in;
  wire [PARALLEL_READ*W-1:0]         weight_out;

  register_file #(
    .DEPTH(DEPTH),
    .ADDR_SIZE(ADDR_SIZE),
    .W(W),
    .PARALLEL_READ(PARALLEL_READ)
  ) dut (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .write_i(write_i),
    .addr_i(addr_i),
    .data_i(weight_in),
    .data_o(weight_out)
  );
  always begin
    #5 clk_i = ~clk_i;
  end

  integer index_write;

  initial begin
    clk_i = 1;
    rst_i = 0;
    write_i = 0;
    addr_i = 0;
    weight_in = 0;

    // Write
    for (index_write = 0; index_write < DEPTH; index_write = index_write + 1) begin
      @(posedge clk_i);
      weight_in = 10 + index_write;
      addr_i = index_write;
      write_i = 1;
    end
    @(posedge clk_i);
    write_i = 0;

    // Read
    for (index_write = 0; index_write < DEPTH; index_write = index_write + 1) begin
      @(posedge clk_i);
      addr_i = index_write;
    end
  end
endmodule
