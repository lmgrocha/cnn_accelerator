
module tb_input_buffer ();
  localparam W = 5;
  localparam DEPTH = 5;
  localparam SHIFT_SIZE = 3;

  // Wires
  reg  clk_i, rst_i;
  reg  parallel_load, shift_load;
  reg  [W-1:0] word_0, word_1, word_2, word_3, word_4, word_5, word_6;
  wire [W*(DEPTH+SHIFT_SIZE-1)-1:0] parallel_data;
  wire [SHIFT_SIZE*W-1:0]           serial_data_i;
  wire [W*(DEPTH+SHIFT_SIZE-1)-1:0] buffer_data;

  input_buffer #(
    .W(W),
    .DEPTH(DEPTH),
    .SHIFT_SIZE(SHIFT_SIZE)
  ) dut (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .parallel_load_i(parallel_load),
    .shift_load_i(shift_load),
    .parallel_data_i(parallel_data),
    .serial_data_i(serial_data_i),
    .data_o(buffer_data)
  );
  
  // Use same interface
  assign serial_data_i = parallel_data[W*(SHIFT_SIZE)-1:0];
  assign parallel_data = {word_6, word_5, word_4, word_3, word_2, word_1, word_0};

  always begin
    #5 clk_i = ~clk_i;
  end

  initial begin
    clk_i = 0;
    rst_i = 1;
    word_0 = 0;
    word_1 = 1;
    word_2 = 5;
    word_3 = -8;
    word_4 = -2;
    word_5 = 7;
    word_6 = -1;
    parallel_load = 0;
    shift_load = 0;
  
    #30 rst_i = 0;
    #10 parallel_load = 1;
    #10 parallel_load = 0;
    #40 word_0 = -15;
    word_1 = 15;
    shift_load = 1;
    #10 shift_load = 0;
    #10 word_0 = 0;
    word_1 = -9;
    word_2 = 0;
    shift_load = 1;
    #10 shift_load = 0;
    #10 word_0 = 1;
    word_1 = -1;
    word_2 = 1;
    shift_load = 1;
    #10 shift_load = 0;
    #10 word_0 = 2;
    word_1 = -2;
    word_2 = 2;
    shift_load = 1;
    #10 shift_load = 0;
    #10 word_0 = 3;
    word_1 = -3;
    word_2 = 3;
    shift_load = 1;
    #10 shift_load = 0;
    #10 word_0 = 4;
    word_1 = -4;
    word_2 = 4;
    shift_load = 1;
    #10 shift_load = 0;
    #10 word_0 = -7;
    word_1 = 7;
    word_2 = 5;
    shift_load = 1;
    #10 shift_load = 0;
    #10 word_0 = -6;
    word_1 = 6;
    shift_load = 1;
    #10 shift_load = 0;
    #10 word_0 = -5;
    word_1 = 5;
    shift_load = 1;
    #10 shift_load = 0;
    #10 word_0 = -4;
    word_1 = 4;
    shift_load = 1;
    #10 shift_load = 0;

  end
endmodule
