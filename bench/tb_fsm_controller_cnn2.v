
module tb_fsm_controller_cnn2 ();
  localparam NUMBER_INPUT_CHANNELS = 32;
  localparam INPUT_PROCESSING_LENGTH = 1000;
  localparam FILTER_SIZE = 40;
  localparam NUMBER_FILTERS = 32;
  localparam RFW_ADDR_SIZE = 5;
  localparam RFW_SIZE = 32;
  localparam FILTER_PAR = 40;
  localparam NUMBER_PE_PAR = 8;
  localparam LOG2_NUMBER_PE_PAR = 3;
  localparam OUTPUT_PAR = 4;
  localparam LOG2_OUTPUT_PAR = 2;
  // Inputs
  reg clk, rst, start_config, data_ready;

  // Outputs
  wire ready, enable_max_pool, load_max_pool, fetch_weight, rfw_write_enable;
  wire [RFW_ADDR_SIZE-1:0]   rfw_addr;
  wire [LOG2_OUTPUT_PAR-1:0] input_buffer_switch;
  wire [OUTPUT_PAR-1:0]      write_accumulator;

  fsm_controller_cnn2 #(
    .NUMBER_INPUT_CHANNELS(NUMBER_INPUT_CHANNELS),
    .FILTER_SIZE(FILTER_SIZE),
    .NUMBER_FILTERS(NUMBER_FILTERS),
    .RFW_ADDR_SIZE(RFW_ADDR_SIZE),
    .RFW_SIZE(RFW_SIZE),
    .FILTER_PAR(FILTER_PAR),
    .NUMBER_PE_PAR(NUMBER_PE_PAR),
    .LOG2_NUMBER_PE_PAR(LOG2_NUMBER_PE_PAR),
    .OUTPUT_PAR(OUTPUT_PAR),
    .LOG2_OUTPUT_PAR(LOG2_OUTPUT_PAR)
  ) dut (
    .clk_i(clk),
    .rst_i(rst),
    .start_config_i(start_config),
    .data_ready_i(data_ready),
    .rfw_write_enable_o(rfw_write_enable),
    .rfw_addr_o(rfw_addr),
    .ready_o(ready),
    .input_buffer_switch_o(input_buffer_switch),
    .write_accumulator_o(write_accumulator),
    .enable_max_pool_o(enable_max_pool),
    .load_max_pool_o(load_max_pool),
    .fetch_weight_o(fetch_weight)
  );

  // Clock generation
  always begin
    #5 clk = ~clk;
  end
  
  initial begin
    clk = 1;
    rst = 1;
    data_ready = 0;
    start_config = 0;

    @(posedge clk);
    @(posedge clk);
    rst = 0;
    @(posedge clk);
    // Start config
    start_config = 1;
    @(posedge ready);
    start_config = 0;
    @(posedge clk);
    @(posedge clk);
    
    // Start data processing
    data_ready = 1;

  end
endmodule
