
import os
import numpy as np

weight_filename = 'weight_memory_cnn2.txt'
input_filename = 'input_memory_cnn2_bin.txt'

filter_size = 5
output_filters = 32
input_channels = 32
input_fm_size = 40
output_fm_size = 10

def binary_mult(data, weight, debug=0):
    binary_weight = '{0:05b}'.format(int(weight))
    accumulator = 0
    for x in range(len(binary_weight)):
        if int(binary_weight[x]) == int(data[x]):
            accumulator += 1

    # Correct popcount
    popcount = accumulator*2 - filter_size
    if debug > 0:
        print(binary_weight, data)
        print(accumulator, popcount)
        print('\n')
    return popcount


# Load both input and weight memories as text
with open(os.path.join(os.getcwd(), weight_filename)) as f:
    weight_lines = f.readlines()

with open(os.path.join(os.getcwd(), input_filename)) as f:
    input_lines = f.readlines()

# Convert weights to a 32x32 weight array
weight_array = np.zeros((output_filters, input_channels))
for index, line in enumerate(weight_lines):
    current_weight = int(line, 2)
    weight_set = index // output_filters
    weight_index = index % input_channels
    weight_array[weight_set][weight_index] = current_weight

# Convert inputs to a 32xN input array
input_array = np.zeros((input_fm_size*filter_size, input_channels))
for index, line in enumerate(input_lines):
    current_inputs = line.strip().split('_')

    print(current_inputs)

    for channel_offset in range(len(current_inputs)):
        input_channel = (index % 16) * 2 + (len(current_inputs)-channel_offset-1)

        for index_offset in range(len(current_inputs[channel_offset])):
            input_index = index_offset + (index // 16) * filter_size
            input_array[input_index][input_channel] = int(current_inputs[channel_offset][index_offset], 2)

# Compute the convolution
output_array = np.zeros((output_fm_size, output_filters))

for index_output in range(output_fm_size):
    for output_channel in range(output_filters):
        current_output = 0
        for input_channel in range(input_channels):
            data = input_array.swapaxes(0, 1)[input_channel][index_output*filter_size:index_output*filter_size+filter_size]
            current_output += binary_mult(data, weight_array[output_channel][input_channel])
        output_array[index_output][output_channel]= current_output

print(output_array)
