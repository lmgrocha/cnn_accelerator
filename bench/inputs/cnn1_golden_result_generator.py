
import os
import numpy as np

weight_filename = 'weight_memory_cnn1.txt'
input_filename = 'input_memory.txt'
threshold_filename = 'threshold_memory.txt'

input_width = 5
filter_size = 5
output_filters = 32
input_channels = 1
input_fm_size = 150
output_fm_size = 140

def binary_filter(data, weight, debug=0):
    binary_weight = '{0:05b}'.format(int(weight))[::-1]
    accumulator = 0
    for x in range(len(binary_weight)):
        if int(binary_weight[x]) == 1:
            accumulator += data[x]
        else:
            accumulator -= data[x]

    if debug > 0:
        print(binary_weight, data)
        print(accumulator)
        print('\n')
    return accumulator

def binarize(data, threshold):
    if data >= threshold:
        return 1
    else:
        return 0

# Load both input and weight memories as text
with open(os.path.join(os.getcwd(), weight_filename)) as f:
    weight_lines = f.readlines()

with open(os.path.join(os.getcwd(), input_filename)) as f:
    input_lines = f.readlines()

with open(os.path.join(os.getcwd(), threshold_filename)) as f:
    threshold_lines = f.readlines()

# Convert weights to a 32x1 weight array
weight_array = np.zeros(output_filters)
for index, line in enumerate(weight_lines):
    current_weight = int(line, 2)
    weight_array[index] = current_weight

# Convert thresholds to a 32x1 array
threshold_array = np.zeros(output_filters)
for index, line in enumerate(threshold_lines):
    current_threshold = int(line, 2)
    threshold_array[index] = current_threshold

# Convert inputs to a 25x1 weight array
input_array = np.zeros(input_fm_size)
for index, line in enumerate(input_lines):
    if index >= input_fm_size:
        break

    current_input = int(line, 2)
    input_array[index] = current_input

# Compute the convolution
conv_array = np.zeros((output_fm_size, output_filters))
binary_array = np.zeros((output_fm_size, output_filters))
max_pooled_array = np.zeros((output_fm_size//4, output_filters))

for index_output in range(output_fm_size):
    for output_channel in range(output_filters):
        current_output = 0
        data = input_array[index_output:index_output+filter_size]
        current_output += binary_filter(data, weight_array[output_channel])
        conv_array[index_output][output_channel]= current_output
        binary_array[index_output][output_channel]= binarize(current_output, threshold_array[output_channel])

# Compute the max pooled version
for output_channel in range(output_filters):
    for index in range(0, output_fm_size-4, 4):
        print(binary_array.swapaxes(0,1)[output_channel][index:index+4])
        max_pool = np.max(binary_array.swapaxes(0,1)[output_channel][index:index+4])
        max_pooled_array[index//4][output_channel] = max_pool
np.savetxt('cnn1_conv_nonbin.txt', conv_array, fmt='%4i')
np.savetxt('cnn1_conv_bin.txt', binary_array, fmt='%i', delimiter='')
np.savetxt('cnn1_conv_bin_maxpooled.txt', max_pooled_array, fmt='%i', delimiter='')

with open(os.path.join(os.getcwd(), '{}_cnn2_bin.txt'.format(input_filename[:-4])), 'w') as f:
    for index_output in range(30):
        for output_channel in range(0, output_filters, 2):
            channel_0 = max_pooled_array.swapaxes(0,1)[output_channel][index_output:index_output+filter_size].astype(int)
            channel_1 = max_pooled_array.swapaxes(0,1)[output_channel+1][index_output:index_output+filter_size].astype(int)
            output_string = '{}_{}\n'.format(''.join(['{}'.format(x) for x in channel_1])[::-1], ''.join(['{}'.format(x) for x in channel_0])[::-1])
            print(index_output, output_string)
            f.write(output_string)
