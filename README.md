# ChillPlus CNN Accelerator

Hardware architecture for convolutional network to be integrated into the ChillPlus network.

**Table of Contents**

+ [Folder Structure](#folder-structure)
+ [Verilog Tips](#verilog-tips-important)
+ [Architecture description](#architecture-description)
    + [Circuit Operation](#circuit-operation)
    + [Input buffer](#input-buffer)
    + [Register file](#register-file)
    + [CNN1](#cnn1)
    + [Binarizer](#binarizer)
    + [Max pooling](#max-pooling)
    + [Memory buffer](#memory-buffer)
    + [Binary filter](#binary-filter)
    + [CNN2](#cnn2)
    + [Top Level](#top-level)
+ [Open Issues](#open-issues)


# Folder Structure
The project is divided into three folders:
+ `rtl`: contains all the described modules required for the implementation.
+ `bench`: contains the unit testbenches for functional verification.
    + `inputs`: contains files whose data are used as inputs for specific testbenches like weights, inputs, etc.
+ `waves`: contains some Xilinx ISIM testbench waveform layouts to facilitate verification.

# Verilog tips (IMPORTANT)
Verilog-2001 does not support array interfaces. This issue makes the code less readable when we need array interfaces.
To overcome this issue, whenever there is an interface which suport  more than one data word in a specific signal, we concatenate these words in a little-endian format.

**Example:** let's asusme a signal ```data``` which should have 4 words of 6 bits. We declare and use this signal as:
```verilog
localparam NUM_WORDS = 4;
localparam WORD_SIZE = 6;
wire [NUM_WORDS*WORD_SIZE-1:0] data;
wire [WORD_SIZE-1:0] packed_data [0:NUM_WORDS-1];

// To simplify, we can 'pack' this data and access it as a vector
genvar i;
generate
  for (i=0; i<NUM_WORDS; i=i+1) begin :gen_pack
    assign packed_data[i] = data[(i+1)*WORD_SIZE-1:i*WORD_SIZE];
  end
endgenerate
```

# Archictecture Description

The design was conceived to have a fully pipelined data stream so all the blocks can operate at the same time.
Given that different modules have different constraints, there are some intermediary memories to handle this data rate mismatch.

<details>
<summary>See more ...</summary>
The overall design architecture can be seen in the picture below:
[INSERT PICTURE RERE]
</details>

## Circuit operation

The circuit operation has three phases after it is reset:

<details>
<summary> See details ...</summary>

### Configuration phase:

In this phase, all the weights and binarization thresholds are loaded into the register files. Within each loading phase, the controller assumes that the data stream will receive data every clock cycle, removing the need for REQ-ACK signaling. The controller assumes the following loading order:

1. CNN1 filter weights (1x 40-bit filter per clock cycle)
2. Binarizer1 thresholds (1x n-bit thresold per clock cycle)
3. CNN2 filter weights (1x 40-bit filter per clock cycle)

   It is important to notice that CNN2 has multiple input and output channels. Hence, each output filter has 32 sub-filters which are specific for each input channel. This is important for loading and data processing scheduling. In this case, the the loading schedule is:

   - (cycle 1) Filter 0, input channel 0 (1x 40-bit filter)
   - (cycle 2) Filter 0, input channel 1 (1x 40-bit filter)
   - (cycle 32) Filter 0, input channel 31 (1x 40-bit filter)
   - (cycle 33) Filter 1, input channel 0 (1x 40-bit filter)
   - (cycle 34) Filter 1, input channel 1 (1x 40-bit filter)
   - (cycle 993) Filter 31, input channel 0 (1x 40-bit filter)
   - (cycle 1024) Filter 31, input channel 31 (1x 40-bit filter)
4. Binarizer2 weights

### Idle phase

Once the configuration is finished, the system enters in **idle mode** until the external memory (controlled in the testbench) indicates that there is data avaialble to be processed.

### Data processing phase

When the external memory has available data, it sends a signal to the circuit which enter in **processing mode**. Everytime that the circuit needs new input data from the external memory, it sends a request and expects the data to be presented **one cycle later**.
</details>

## Module hierarchy (RTL files)
```bash
|-- top_chillplus.v
|   |-- fsm_top.v
|   |-- cnn_layer1.v
|   |   |-- fsm_controller_cnn1.v
|   |   |-- register_file.v
|   |   |-- input_buffer.v
|   |   |-- multiplier_bin_weight.v
|   |   |-- adder_tree.v
|   |-- max_pool_optimized.v
|   |-- binarizer.v
|   |   |-- register_file.v
|   |-- memory_buffer.v
|   |   |-- reg_serial_to_parallel.v
|   |-- cnn_layer2.v
|   |   |-- fsm_controller_cnn2.v
|   |   |-- register_file.v
|   |   |-- binary_filter.v
|   |   |-- accum_tree.v
|   |   |-- reg_n_bits.v

```

## Input buffer

<details>
<summary>See description</summary>

We use a FIFO-based input buffer to store the data fetched from the external memory. It helps reducing the external memory bandwidth.

Further, it allows parallel and adjustable serial load. The serial load can be configured to load more than one word at time. The actual buffer size is always given by ```DEPTH+SHIFT_SIZE-1```. 

### Module architecture

_TODO_: include module architecture picture.

### Configurable Parameters

+ ```W```: width of each stored word.
+ ```DEPTH```: number of stored words.
+ ```SHIFT_SIZE```: defines how many word should be loaded on serial mode.

### Interface definition

+ ```clk_i```: input clock
+ ```rst_i```: reset signal (active high).
+ ```parallel_load_i```: parallel load enable (active high).
+ ```shift_load_i```: shift load enable (active high).
+ ```parallel_data_i```: parallel data interface. Size is given by ```(DEPTH+SHIFT_SIZE-1)*W```.
+ ```serial_data_i```: serial data interface. Size is given by ```SHIFT_SIZE*W```.
+ ```data_o```: output data interface. Size is given by ```(DEPTH+SHIFT_SIZE-1)*W```.
</details>

## Register file

<details>
<summary>See description</summary>

This module implements a register file to be used in several modules. 

Although it has a reset signal, it does not use it (no initialization required) since it assumes that in the ASIC implementation it will be implemented as an IPCORE-based circuit.

### Module architecture

_TODO_: include module architecture picture.

### Configurable Parameters

+ ```W```: width of each stored word.
+ ```DEPTH```: number of stored words.
+ ```ADDR_SIZE```: address size to access the register. Should be ```log2(DEPTH)```.
+ ```PARALLEL_READ```: defines how many sequential register will be put on the output. Default is 1.

### Interface definition

+ ```clk_i```: input clock.
+ ```rst_i```: reset signal (active high). **not used**
+ ```write_i```: write input data to specific address (active high).
+ ```addr_i```: register selection address. 
+ ```data_i```: input data interface.
+ ```data_o```: output data interface. Size is given by ```PARALLEL_READ*W```.
</details>

## CNN1

<details>
<summary>See description</summary>

### Module architecture

_TODO_: include module architecture picture.

### Interface definition

### Configurable Parameters

</details>

## Binarizer

<details>
<summary>See description</summary>

This module binarizes the input according to a threshold obtained during training time. Everytime an output is ready at the CNN, it should be sent to the binarizer block with the associated output filter address to select the right threhsold.

This threshold is computed from three parts:

1. The scaling factor (alpha) of the filters. During training binarization, filters are reduced to ```alpha*[+1, -1, -1, ... +1]```.
2. The batch normalization parameters (gamma, mu, beta, sigma)
3. The ReLU function

Theoretically, the threshold should be given as:

```
y = (x*gamma*alpha)/(sigma + epsilon) + (beta - mu/(sigma +epsilon))
hence:
x >= (beta - mu/(sigma + epsilon))*((sigma + epsilon)/(gamma*alpha))
```

### Module architecture

_TODO_: include module architecture picture.

### Configurable Parameters

+ ```THRESHOLD_WIDTH```: threshold bitwidth.
+ ```INPUT_WIDTH```: input bitwidth. Should be equal to the accumulator size of the CNN.
+ ```NUMBER_FILTERS```: number of filters which indicates the number of thresholds to store.
+ ```LOG2_NUMBER_FILTERS```: address size. Should be ```log2(NUMBER_FILTERS)```.

### Interface definition

+ ```clk_i```: clock signal.
+ ```rst_i```: reset signal (active high). **not used**
+ ```load_thresh_i```: write signal to store current threshold to specific address (active high).
+ ```thres_value_i```: input threshold value.
+ ```thres_addr_i```: threshold register address. Used both for write and read.
+ ```input_value_i```: CNN output value which will be binarized. 
+ ```binary_o```: binarized output.
</details>

## Max Pooling

<details>
<summary>See description</summary>

This unit computes the max poling of the binarized CNN outputs. The signals used to control this module are generated by the FSM of the associated CNN.

For a 4-input max pooling, the following control sequence has to be followed:
1. Load the first input (load flag) to the pooling unit and keep it enabled.
2. Feed the data for the next three cycles with load flag deactivated.
3. At the 4th cycle, the output is the max pooling of past 4 samples. New data should be loaded by activating the load flag.

### Module architecture

_TODO_: include module architecture picture.

### Configurable Parameters

None

### Interface definition

+ ```clk_i```: clock signal.
+ ```rst_i```: reset signal (active high).
+ ```enable_i```: active the pooling unit (active high). Every cycle it compares current input with previous one.
+ ```load_i```: Load the internal register with new data withou any comparison.
+ ```data_i```: input data.
+ ```data_o```: max-pooled data.
</details>

## Memory buffer

<details>
<summary>See description</summary>

To generate a single input for CNN2, the CNN1 module has to generate 4 outputs which are subsequently max pooled. It means that every four cycles there is one output value for one output filter. Since CNN1 uses **input reuse**, it first computes one row for all filters before computing the next row (i.e., it stores data in a row-wise fashion).

However, the CNN2 needs to read multiple data of a single filter (i.e., column-wise reading). Hence, the memory buffer acts as a transposition buffer to pipeline the operation of CNN1 and CNN2. It performs this operation using a row buffer and a transposition memory. For better illustration, let's assume the following:
    1. CNN1 has 32 output filters.
    2. A max-pooled output is generated every four cycles.
    3. The CNN2 filter size is equal to 40.

In this case, the memory buffer architecture would be:
    + A row buffer of size 1x32 bits (1 bit for each filter)
    + A memory of size 40x32 bits (40 bits for each feature map)

The row buffer is filled every **32x4 = 128** cycles. Once it happens, its content is written to the memory in a row-wise fashion using a FIFO approach (the contents of the memory are shifted to the next position once new data is ready).

Once the memory is full, it indicates to CNN2 that there is data available and the processing can start. All the data has to be fully used in 128 cycles which is the time until new data arrives to the memory.

The memory buffer has an embedded read and write control.

### Module architecture

_TODO_: include module architecture picture.

### Configurable Parameters

+ ```WIDTH```: defines memory width (aka, number of output channels of CNN1).
+ ```DEPTH```: defines memory depth (aka, the filter size of CNN2).
+ ```PARALLEL_EXT```: defines the number of extra values to store for each filter to consider parallel output on CNN2.

### Interface definition

+ ```clk_i```: clock signal.
+ ```rst_i```: reset signal (active high).
+ ```write_buffer_i```: buffer write enable (active high).
+ ```data_i```: input data
+ ```fetch_memory_data_i```: memory read signal (active high).
+ ```data_ready_o```: indicates when the memory data is available (active high).
+ ```data_o```: output data.
</details>

## Binary filter

<details>
<summary>See description</summary>

The binary filter implements the popcount multiplication since both inputs and weights are binary.
Its works as follows:
1. Compute the XNOR operation between input data and weights.
2. Count how many bits are equal to '1' on the XNOR result.
3. Correct the popcount bias (2*popcount - Size input (bits))

For further understanding of the popcount algorithm, please refer to [this website](https://sushscience.wordpress.com/2017/10/01/understanding-binary-neural-networks/).

### Module architecture

_TODO_: include module architecture picture.

### Configurable Parameters

+ ```FILTER_SIZE```: defines the filter size.
+ ```LOG2_FILTER_SIZE```: defines how many bits are required for the filter sum.

### Interface definition

+ ```data_i```: input data.
+ ```weights_i```: weights to be multiplied.
+ ```filter_o```: multiplication result according to the popcount algorithm.
</details>

## CNN2

<details>
<summary>See description</summary>

This block computes all the convolutions for the second CNN layer on the ChillPlus.

Each filter output requires INPUTS_CHANNELS x FILTER_SIZE operations.

The buffer/memory between CNN1 and CNN2 enables pipelining the operation of both layers.

The buffer will take 128 (4x32) cycles to write a new data to the memory that will be used by CNN2, considering
that CNN2 is set with OUTPUT_PAR = 1.

In this case, CNN2 has to process INPUT_CHANNELS x FILTER_SIZE x OUTPUT_CHANNELS operations in 128 cycles.

Considering the following case:
  - ```INPUT_CHANNELS = 32```
  - ```FILTER_SIZE = 40```
  - ```OUTPUT_CHANNELS = 32```

the number of operations to be executed is 32x40x32 = 40960 => 40960/128 = 320 operations/cycle.
Hence, CNN2 needs to compute 8 filter computations per cycle (8x40 = 320).

We adopted a hybrid approach to allocate the PE computation to balance the weight and data memories.
Also, we adopted ```OUTPUT_PAR=1``` to reduce the memory requirements.

### Module architecture

_TODO_: include module architecture picture.

### Configurable Parameters

### Interface definition

</details>

## Top level

<details>
<summary>See description</summary>
</details>

# Open Issues

- [ ] Add description of remaining modules
- [ ] Create figures for the architecture and its submodules
- [ ] Merge LSTM repository into this one
- [ ] Finish verification of CNN2
- [ ] Finish the integration of all convolution modules
- [ ] Move binarizer to before max pooling unit on the top-level

